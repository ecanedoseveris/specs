<div margin="0px auto" style="display: inline; clear: both;">
  <img src="assets/img/logo.png" width="40%" style="border: 1px solid 000;"/>
</div>


# Frameworks

If framework of Soter use InAuth or EADS modules is completely necessary add this external frameworks for run, this Frameworks can be downloaded [here](https://gitlab.com/ecanedoseveris/specs/-/tree/master/frameworks)

For Soter with InAuth module
```
InMobileMME.framework
```
For Soter with EADS module
```
Cordova.framework
NfcOnboardingApp.framework
OpenSSL.framework
```

This frameworks needs to remove an inactive archs when we try to distribute app, it need to add the next script into _Build Phases_:
```
APP_PATH="${TARGET_BUILD_DIR}/${WRAPPER_NAME}"

find "$APP_PATH" -name '*.framework' -type d | while read -r FRAMEWORK
do
FRAMEWORK_EXECUTABLE_NAME=$(defaults read "$FRAMEWORK/Info.plist" CFBundleExecutable)
FRAMEWORK_EXECUTABLE_PATH="$FRAMEWORK/$FRAMEWORK_EXECUTABLE_NAME"
echo "Executable is $FRAMEWORK_EXECUTABLE_PATH"
echo $(lipo -info "$FRAMEWORK_EXECUTABLE_PATH")

FRAMEWORK_TMP_PATH="$FRAMEWORK_EXECUTABLE_PATH-tmp"

case "${TARGET_BUILD_DIR}" in
*"iphonesimulator")
    echo "No need to remove archs"
    ;;
*)
    if $(lipo "$FRAMEWORK_EXECUTABLE_PATH" -verify_arch "i386") ; then
    lipo -output "$FRAMEWORK_TMP_PATH" -remove "i386" "$FRAMEWORK_EXECUTABLE_PATH"
    echo "i386 architecture removed"
    rm "$FRAMEWORK_EXECUTABLE_PATH"
    mv "$FRAMEWORK_TMP_PATH" "$FRAMEWORK_EXECUTABLE_PATH"
    fi
    if $(lipo "$FRAMEWORK_EXECUTABLE_PATH" -verify_arch "x86_64") ; then
    lipo -output "$FRAMEWORK_TMP_PATH" -remove "x86_64" "$FRAMEWORK_EXECUTABLE_PATH"
    echo "x86_64 architecture removed"
    rm "$FRAMEWORK_EXECUTABLE_PATH"
    mv "$FRAMEWORK_TMP_PATH" "$FRAMEWORK_EXECUTABLE_PATH"
    fi
    ;;
esac

echo "Completed for executable $FRAMEWORK_EXECUTABLE_PATH"
echo $(lipo -info "$FRAMEWORK_EXECUTABLE_PATH")

done
```

# Integrate Soter SDK into a existing project

## Requirements

Xcode 12.2 is required if static library includes a EADS framework.\
Xcode 11.0+ is required if static library not includes EADS framework.

### CocoaPods

Make sure you have the [CocoaPods](http://cocoapods.org) dependency manager installed. You can do so by executing the following command:

```sh
$ gem install cocoapods
```

Example podfile:

```txt
use_frameworks!

# Soter SDK
target "<Your Target Name>" do
  pod 'Soter'
  # Optional: uncomment to install control of notifications
  # pod 'SoterMessaging'
end
```

Install using the following command:

```sh
$ pod install
```

And open the .xcworkspace file to see the project in Xcode.

```sh
$ open your-project.xcworkspace
```

Integrate external necessary [frameworks](#frameworks)
### Integrate whitout CocoaPods

If you don't want to use Cocoapods, you can still take advantage of the Soter SDKs by importing the frameworks directly.

1. Download the latest compilation mode that you want, for example [0.0.1](https://gitlab.com/ecanedoseveris/specs/-/blob/master/Soter/0.0.1/Soter-0.0.1.zip)
2. Unzip de file and move/copy the framework into your project.
3. Integrate external necessary [frameworks](#frameworks)

## Quickstart

### Capabilities

If install the library with EADS you need to enable _Near Field Communication Tag Reading_ mode under the capabilities section for the main application target.

### Info.plist

If framework with EADS is installed must be added the following _Privacy permissions_ in Info.plist

```
  <key>NFCReaderUsageDescription</key>
  <string>Check passport</string>
  <key>NSCameraUsageDescription</key>
  <string>This app wants to take pictures and check passport.</string>
  <key>NSMicrophoneUsageDescription</key>
  <string>This app wants to user your microphone and validate biometry.</string>
```
I framework has module InAuth must be added

```
<key>NSLocationWhenInUseUsageDescription</key>
<string>This app wants to user your location and validate KYD.</string>
```

## Initialize Soter Messaging in your app

1. Import the SoterMessaging module in your ``UIApplication`` (_optional_ if you need to change language or initialization)

```
# Optional
import SoterMessaging
```
2. Exampled integrated with ``Firebase Cloud Messaging``, when ``Firebase`` has a valid token is necessary to send this token to server, we use the singleton ``default``of ``SoterMessagingManager`` and the method ``saveToken`` to save it:

```
Messaging.messaging().token { token, error in
    if let error = error {
        print("Error fetching FCM registration token: \(error)")
    } else if let token = token {
        SoterMessagingManager.default.saveToken(token)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure(let error):
                    print("SOTER - Error when send token to server \(error)")
                default: break
                }
            }) { _ in
                print("SOTER - Token saved")
            }.store(in: &self.cancellable)
    }
}
```

3. When the app retrieve a notification they need sended to framework:

```
extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void)
    {
        SoterMessagingManager.default.retrieveNotification(response: response)
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        SoterMessagingManager.default.retrieveNotification()
        completionHandler([.alert, .badge, .sound])
    }
}
```

## Initialize Soter in your app

1. Import the Soter module in your ``UIApplication`` (_optional_ if you need to change language or initialization)

```
# Optional
import Soter
```
2. Configure ``SoterManager`` shared instance, typically in your app's ``application:didFinishLaunchingWithOptions:`` method:

```
# Optionals
# SoterManager.default.modalPresentationStyle = .fullScreen
# SoterManager.default.language = Locale(identifier: "en")
```

3. Configure ``SoterManager`` security JWT (if is needed), typically in your app's ``application:didFinishLaunchingWithOptions:`` method.

You can configure an authentication with JWT for public or private calls. The ``SoterManager`` has a variable called ``jwt`` that can be initialized with the public class SoterJWT and SoterJWT for every scope configuration for public and private (private configuration is optional).

For example:
```
let publicScope = SoterJWTScope(clientID: "obp-onboarding",
                                clientSecret: "b0fcbd-6bfe-4077-b0e3-7f1585485818",
                                grantType: "clientredentials",
                                scope: "obp_scope")
let privateScope = SoterJWTScope(clientID: "obp_mobile",
                                  clientSecret: "e5e7f86c-38bf-470e-b540-686781a304bc",
                                  grantType: "password",
                                  scope: "accounts")
SoterManager.default.jwt = SoterJWT(public: publicScope, private: privateScope)
```
The private scope token will be created after customer had been created.

4. Add configuration file for setup appearance and submodules configuration in Soter (inAuth and EADS are _optionals_). This configuration it will be sent with the instructions to append Soter framework in your app, for example:

```
{
  "onboardingID": "74075f9e-0028-4840-8f98-ca62e585fbc5",
  "entityID": "ecb4e73d-b453-4319-8af1-2adaf59d99d8",
  "phoneContact": "7867583",
  "inAuth": {
    "url": "https://api.stg.obpnttdata.com/on-boarding/customers/",
    "accountID": "08c19983-eb62-42fc-a6e9-adb1aaadea17",
    "applicationID": null,
    "serverKeysFileName": "server_keys"
  },
  "EADS": {
    "userID": "soter",
    "password": "bi0$oter2020",
    "defaultLanguage": "en",
    "voiceCaptureOption": "yes"
  },
  "electronicId": {
    "url": "https://etrust-sandbox.electronicid.eu/v2",
    "registrationAuthorityId": "b470af5d-5b74-48b7-81a4-1eb6251b631b",
    "bearerId": "e04e5b1f-e0f8-432f-93f4-522bc314a7d7",
    "tenantId": "84c006c3-7f64-4f60-ad8c-91b64789b516",
    "language": "en",
    "authIdType": "62"
  },
  "style": {
    "colors": {
        "primary": "#6785c1",
        "secondary": "#0080b1",
        "accent": "#6785c1",
        "alert": "#b71c1c",
        "background": "#ffffff",
        "buttonInactiveColor": "#C1C1C1",
        "buttonActiveColor": "#6785C1",
        "buttonTextInactiveColor": "#FFFFFF",
        "buttonTextActiveColor": "#FFFFFF",
        "buttonRadius": 3.0,
        "logo": "#FFFFFF",
        "navigation": "#6785c1"
    },
    "transparentHeader": false,
    "logo": "logo-ntt",
    "backgroundImage": null,
    "fontFamily": "SFProDisplay"
  },
  "hint": [{
      "key": "BASIC_DATA",
      "text": [{
              "localizedText": "In this first step, we capture some basic data in order to allow you to resume the process at the point where you were.",
              "language": "en"
          },
          {
              "localizedText": "En este primer paso, capturamos unos datos básicos con el objetivo de permitirle retomar el proceso en el punto en el que se encontraba.",
              "language": "es"
          },
          {
              "localizedText": "この最初のステップでは、何らかの理由でプロセスを放棄した場合に",
              "language": "ja"
          }
      ]
  }]
}
```

5. If module InAuth is active is necessary to copy a file named ``server_keys.json`` into project for manage the register and logs from this library, if not exist the module produces an error and it's not possible to continue with the flow.

## Usage

### Launch

#### New Customer

If we initiated the new process we need to call a ``initProcess`` method, this method has information about  ``configFile`` is _optional_ (by default it loaded a file called soter-config.json), ``navigation`` is the navigation parent where launch Soter and ``loadingView`` is _optional_ for show when app is processing a request.
```
# If we launch Soter default views
SoterManager.default.defineDefault()
SoterManager.default.initProcess(configFile: "soter-custom", navigation:  navigationController, loadingView: LoadingView(), completion: { [weak self] in
    // hideLoading
}) { [weak self] error in
    // showError && hideLoading
}
```

#### Login 

Before launch Soter in method ``login`` we need a ``username`` and ``password`` to create a custom flow for this customer (this value should be a unique identification for customer), ``configFile`` is _optional_ (by default it loaded a file called soter-config.json), ``navigation`` is the navigation parent where launch Soter and ``loadingView`` is _optional_ for show when app is processing a request.
```
# If we launch Soter default views
SoterManager.default.defineDefault()
# Login process to onboarding
SoterManager.default.continueProcess(username: "465556667Q", password: password, configFile: file, navigation: navigationController, loadingView: LoadingView(), completion: { [weak self] in
    // hideLoading
}) { [weak self] error in
    // showError && hideLoading
}
```

### Orientation

When framework has EADS module, we need to allow a landscape mode for get all data from Identifier or Passport, Soter framework return if we need to rotate, for example:

```
func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
    return SoterManager.default.orientation ?? .portrait
}
```

### Colors Customizations

The aim is to allow the onboarding to be as customizable as possible without making it overly complex and bloated.

In your config json file in _style_ node, you can change any value of default Soter appearance.
```
"style": {
  "colors": {
    "primary" : "#145B64",
    "secondary" : "#1995A4",
    "accent" : "#F19920",
    "background" : "#FFFFFF",
    "buttonInactiveColor": "#1995A4",
    "buttonActiveColor": "#F19920",
    "buttonTextInactiveColor": "#FFFFFF",
    "buttonTextActiveColor": "#FFFFFF",
    "buttonRadius": 3.0,
    "logo" : "#FFFFFF",
    "navigation" : "#145B64"
  },
  "transparentHeader": false,
  "logo": null,
  "backgroundImage": null,
  "fontFamily": "SFProDisplay"
}
```
### Hints Customizations

You can add a modal information in selected modules (BASIC_DATA, KYC, BACKOFFICE_VALIDATION, SIGNATURE)

In your config json file in _hint_ node, you can remove or add any information about module and the app automatically show there info.
```
"hint": [{
      "key": "BASIC_DATA",
      "text": [{
              "localizedText": "In this first step, we capture some basic data in order to allow you to resume the process at the point where you were.",
              "language": "en"
          },
          {
              "localizedText": "En este primer paso, capturamos unos datos básicos con el objetivo de permitirle retomar el proceso en el punto en el que se encontraba.",
              "language": "es"
          },
          {
              "localizedText": "この最初のステップでは、何らかの理由でプロセスを放棄した場合に",
              "language": "ja"
          }
      ]
  }]
```

#### Colors equivalences in examples:

<div margin="0px auto" style="display: inline; clear: both;">
  <div style="display: inline;">
    <img src="assets/img/config_1.png" width="40%" style="border: 1px solid 000;"/>
  </div>
  <div style="margin-top: 10px;">
    <img src="assets/img/config_2.png" width="40%" style="border: 1px solid 000;"/>
  </div>
  <div style="margin-top: 10px;">
    <img src="assets/img/config_3.png" width="40%" style="border: 1px solid 000;"/>
  </div>
  <div style="margin-top: 10px;">
    <img src="assets/img/config_4.png" width="40%" style="border: 1px solid 000;"/>
  </div>
  <div style="margin-top: 10px;">
    <img src="assets/img/config_5.png" width="40%" style="border: 1px solid 000;"/>
  </div>
  <div style="margin-top: 10px;">
    <img src="assets/img/config_6.png" width="40%" style="border: 1px solid 000;"/>
  </div>
  <div style="margin-top: 10px;">
    <img src="assets/img/config_7.png" width="40%" style="border: 1px solid 000;"/>
  </div>
  <div style="clear: both"/>
</div>


# Outdated

## Advanced view customizations

It's possible to create an entire flow with views implemented from container app. This views are defined in `SoterViewType` and with his delegate we can create a custom view for this type and send data to server.

In every custom view you need to define `SoterViewType` with the delegate `SoterViewDelegate` in your custom class (example with `personalData` type):
```
// MARK: - SoterViewDelegate

extension PersonalDataViewController: SoterViewDelegate {

    var soterType: SoterViewType {
        return .personalData
    }

    var viewController: UIViewController {
        return self
    }

}
```

All `ControllerDelegates` needed the implementation of their methods to take errors (`soterDidFail`) and shows any information or control results (`soterResult`) for finalize the process or show an error result.

At the moment the `ControllerDelegates` that exist are:
- [SoterPersonalDataControllerDelegate](#soterpersonaldatacontrollerdelegate)
- [SoterKYCControllerDelegate](#soterkyccontrollerdelegate)
- [SoterReviewControllerDelegate](#soterreviewcontrollerdelegate)
- [SoterSignControllerDelegate](#sotersigncontrollerdelegate)

### SoterPersonalDataControllerDelegate

Define `soterType` like `.personalData`, delegate your class with `SoterPersonalDataControllerDelegate` and use function `save` for send data to server.

```
func save(email: String, prefix: Int, phone: Int)
```

##### Example:
```
final class PersonalDataViewController: UIViewController, SoterPersonalDataControllerDelegate {

  private func saveUser() {
      save(email: "info@soter.com", prefix: 34, phone: 67777777)
  }

  func soterDidFail(type: SoterError) {
      showAlert(error: type)
  }

  func soterResult(type: SoterResultViewType) {
      switch type {
      case .nok:
          print("nok")
      case .ok:
          print("ok")
      case .waiting:
          print("waiting validation")
      }
  }
```
### SoterKYCControllerDelegate

Define `soterType` like `.kyc`, delegate your class with `SoterKYCControllerDelegate` and use function `save` for send data to server.

```
func save(mappable: Encodable)
```

Mappable is a json `string` from dictionary `[String: AnyEncodable]` (`AnyEncodable` is a public class on Soter), the allowed `keys` and possible types `values` are defined by the server, default example:

```
{
  "accountUsageDescription": "string",
  "operateYourOwnMoney": true,
  "publicActivity": true,
  "relativesPublicActivity": true,
  "sourceOfIncome": "string",
  "sourceOfIncomeDescription": "string",
  "accountUsage": "string",
  "workingStatus": "string",
  "occupation": "string",
  "payerName": "string",
  "frequentOperations": [
    "SENDING_OF_FUNDS",
    "INTERNATIONAL_TRANSFERS",
    "CASH_DEPOSITS",
    "CURRENCY_TRADING",
    "OTHERS"
  ]
}
```

##### Example:
```
final class KYCDataViewController: UIViewController, SoterKYCControllerDelegate {

  private func saveKYC() {
      var dictionary: [String: AnyEncodable] = [:]
      dictionary["accountUsageDescription"] = AnyEncodable(value: "string")
      dictionary["operateYourOwnMoney"] = AnyEncodable(value: true)
      dictionary["frequentOperations"] = AnyEncodable(value: ["SENDING_OF_FUNDS"])
      if let data = try? JSONEncoder().encode(dictionary) {
          save(mappable: String(data: data, encoding: .utf8))
      }
  }

```
### SoterReviewControllerDelegate

Define `soterType` like `.review`, delegate your class with `SoterReviewControllerDelegate` and use function `readed` when user readed document.

```
func readed()
```

##### Example:
```
final class ReviewViewController: UIViewController, SoterReviewControllerDelegate {

  private func save() {
      readed()
  }
```
### SoterSignControllerDelegate

Define `soterType` like `.signVoice`, delegate your class with `SoterSignControllerDelegate` and use function `validate` to send data audio **without base64** code to server.

```
func validate(audio: Data)
```

##### Example:
```
final class VoiceViewController: UIViewController, SoterSignControllerDelegate {

  private func save() {
      guard let fileAudio = URL(string: "/path/to/audio"),
          let data = try? Data(contentsOf: fileAudio) else { return }

      validate(audio: data)
  }
```
### Add custom flow into framework
Add your `SoterViewDelegate` classes into array and use method `define` to set new values, the framework **remove all** view with type repeated (get the first) and views that they should not be by configuration.
```
SoterManager.default.define(steps: [PersonalDataViewController(), VoiceViewController()])
```
If your framework contains module EADS you can add the view in your custom flow through a public class `EADSView`, this view isn't customizable and get the configuration in config file `EADS` node, example:
```
SoterManager.default.define(steps: [PersonalDataViewController(), EADSView()])
```
## Author
Enrique Canedo, [ecanedos@everis.com](mailto://ecanedos@everis.com)

## License
