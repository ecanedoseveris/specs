Pod::Spec.new do |spec|
  spec.name             = "SoterMessaging"
  spec.version          = "1.0.0"
  spec.summary          = "A short description of Soter Messaging"
  spec.homepage         = "https://gitlab.com"
  spec.license          = { :type => "MIT", :file => "LICENSE" }
  spec.author           = { "Everis" => "ecanedos@everis.com" }
  spec.platform         = :ios, "13.0"
  spec.swift_version    = "5.0"
  spec.static_framework = true
  spec.source_files     = "SoterMessaging", "SoterMessaging/**/*.{h,m,swift}"
  spec.source           = { :http => "https://gitlab.com/ecanedoseveris/specs/raw/master/SoterMessaging/1.0.0/SoterMessaging-1.0.0.zip" }
  spec.ios.framework 	= 'SystemConfiguration'
  spec.weak_framework 	= 'UserNotifications'
  spec.ios.vendored_frameworks = ['SoterMessaging.framework']
end
