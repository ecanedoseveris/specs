Pod::Spec.new do |spec|
  spec.name             = "Soter"
  spec.version          = "1.0.0"
  spec.summary          = "A short description of Soter"
  spec.homepage         = "https://gitlab.com"
  spec.license          = { :type => "MIT", :file => "LICENSE" }
  spec.author           = { "Everis" => "ecanedos@everis.com" }
  spec.platform         = :ios, "13.0"
  spec.swift_version    = "5.0"
  spec.static_framework = true
  spec.source           = { :http => "https://gitlab.com/ecanedoseveris/specs/raw/master/Soter/1.0.0/Soter-1.0.0.zip" }
  spec.source_files     = "Soter", "Soter/**/*.{h,m,swift}"
  spec.resource_bundles = { 'Soter' => ['Soter/App/Resources/**/*.lproj', 'Soter/**/*.xcassets', 'Soter/**/*.{png,jpeg,jpg,html,json,mp3}'] }
  spec.exclude_files    = ['Soter/docs', 'Soter/Core/Frameworks', 'Soter/Core/Manager/*EADS.swift', 'Soter/Domain/**/*EADS.swift', 'Soter/Core/Manager/*InAuth.swift' ]
  ## In Development and remove Framework in App Demo
  spec.ios.vendored_frameworks = ['Soter.framework', 'Soter/Core/Frameworks/InMobileMME.framework', 'Soter/Core/Frameworks/OpenSSL.framework', 'Soter/Core/Frameworks/Cordova.framework', 'Soter/Core/Frameworks/NfcOnboardingApp.framework']
  ## To Distribute and add Framework in App Demo
  # spec.ios.vendored_frameworks = ['Soter.framework']
  spec.dependency 'VideoID' , '~> 7.1.9'
end
