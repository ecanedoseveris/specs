/*
 * Copyright © 2019 InAuth, Inc. All rights reserved.
 * The InAuth logo, and other trademarks, service marks, and designs are registered or unregistered
 * trademarks of InAuth, Inc. and its subsidiaries in the United States and in other countries.
 * All other trademarks are property of their respective owners.
 */

#import "MME.h"
#import "InAuthenticateMessage.h"

/**
 The InAuthenticateManager class provides MME class functionality as well as that of the InAuthenticateManager product. To make use of the InAuthenticate features, you will need an InAuthenticate license.  The methods in the InAuthenicateManager class dot provide any network services.
 */
NS_ASSUME_NONNULL_BEGIN
@interface InAuthenticateManager : MME

/**
 Generates a registration `OpaqueObjectRef` payload that can contain a user-defined custom log to be sent to the server prior to any logs or requests. The server will respond with a registration response `OpaqueObjectRef` which should be passed to the `handlePayload` method to complete the registration process.
 
 @param error The error object used to pass error messages back to the caller. The error parameter may be `nil`.
 
 @return A registration `OpaqueObjectRef` hex byte array to be sent to the server.
 */
- (OpaqueObjectRef) generateRegistrationPayload:(NSError **)error __attribute__((swift_error(nonnull_error)));

/**
 Generates a registration `OpaqueObjectRef` payload that can contain a user-defined custom log to be sent to the server prior to any logs or requests. The server will respond with a registration response `OpaqueObjectRef` which should be passed to the `handlePayload` method to complete the registration process.
 
 @param deviceToken A string representing the deviceToken consumed by Firebase or other APNS system to send messages to the system.
 @param error The error object used to pass error messages back to the caller. The error parameter may be `nil`.
 
 @return A registration `OpaqueObjectRef` hex byte array to be sent to the server.
 */
- (OpaqueObjectRef) generateRegistrationPayloadWithDeviceToken:(nullable NSString *)deviceToken
                                                                onError:(NSError **)error __attribute__((swift_error(nonnull_error)));

/**
 Generates a registration `OpaqueObjectRef` payload that can contain a user-defined custom log to be sent to the server prior to any logs or requests. The server will respond with a registration response `OpaqueObjectRef` which should be passed to the `handlePayload` method to complete the registration process.
 
 @param customLog An objective-c `NSDictionary` or swift dictionary containing custom data to log. The `customLog` parameter may be `nil`.
 @param error The error object used to pass error messages back to the caller. The error parameter may be `nil`.
 
 @return A registration `OpaqueObjectRef` hex byte array to be sent to the server.
 */
- (OpaqueObjectRef) generateRegistrationPayloadWithCustomLog:(NSDictionary *)customLog
                                                              onError:(NSError **)error;

/**
 Generates a registration `OpaqueObjectRef` payload that can contain a user-defined custom log to be sent to the server prior to any logs or requests. The server will respond with a registration response `OpaqueObjectRef` which should be passed to the `handlePayload` method to complete the registration process.
 
 @param customLog An objective-c `NSDictionary` or swift dictionary containing custom data to log. The `customLog` parameter may be `nil`.
 @param error The error object used to pass error messages back to the caller. The error parameter may be `nil`.
 
 @return A registration `OpaqueObjectRef` hex byte array to be sent to the server.
 */
- (OpaqueObjectRef) generateRegistrationPayloadWithDeviceToken:(nullable NSString *) deviceToken
                                                              customLog:(nullable NSDictionary *)customLog
                                                                onError:(NSError **)error __attribute__((swift_error(nonnull_error)));

/**
 Determines whether or not the deviceToken sent as a parameter is the same as what has been submitted previously with the current registration.
 
 @param deviceToken A string representing the deviceToken consumed by Firebase or other APNS system to send messages to the system.
 @param error The error object used to pass error messages back to the caller. The error parameter may be `nil`.
 
 @return `YES` if the device token is updated, or `NO` otherwise.
 */
- (BOOL)isDeviceTokenUpdated:(nullable NSString *)deviceToken
                     onError:(NSError **)error __attribute__((swift_error(nonnull_error)));

/**
 Generates a payload to update the device token. This will be used when the device token is to be changed.
 
 @param deviceToken A string representing the deviceToken consumed by Firebase or other APNS system to send messages to the system.
 @param error The error object used to pass error messages back to the caller.
 */
- (OpaqueObjectRef)generateUpdateDeviceTokenPayloadWithDeviceToken:(nullable NSString *)deviceToken
                                                           onError:(NSError **)error __attribute__((swift_error(nonnull_error)));

/**
 Generates an `OpaqueObjectRef` payload containing all collectable logs specified in the logChoices parameter and, if provided, a custom log and transaction ID.
 
 > Note: A log will only be collected if it is both specified in the logChoices parameter and present in the current InMobile LogConfig list retrieved from the server. If no InMobile LogConfig list update has been previously made, then all specified logs will be collected.
 
 > Note: An InMobileListSetLogConfig list is set and retrieved from the server.
 
 @param logChoices An `InMobileLogSet` type of any or all of the available log choices.
 @param customLog An `NSDictionary` containing custom data to log. The customLog parameter may be `nil`.
 @param transId Provides a mechanism to group multiple log transmissions into a customer-defined transaction. Can be set to any string of length 0 to 255. The transId parameter may be `nil`.
 @param error The error object used to pass error messages back to caller. The error parameter may be `nil`.
 
 @return An `OpaqueObjectRef` hex byte array containing a set of logs and/or a custom log to be sent to the server.
 */
- (OpaqueObjectRef) generateLogPayloadFor:(InAuthenticateLogSet)logChoices
                            withCustomLog:(nullable NSDictionary *)customLog
                            transactionId:(nullable NSString *)transId
                                  onError:(NSError **)error __attribute__((swift_error(nonnull_error)));
    
/**
 Generates an `OpaqueObjectRef` payload containing all collectable logs specified in the `logChoices` parameter.
 
 > Note: A log will only be collected if it is both specified in the `logChoices` parameter and present in the current InMobile LogConfig list retrieved from the server. If no `InMobileLogConfig` list update has been previously made, then all specified logs will be collected.
 
 > Note: An InMobile LogConfig list is set and retrieved from the server.
 
 @param logChoices An `InMobileLogSet` type of any or all of the available log choices.
 
 @return An `OpaqueObjectRef` hex byte array containing a set of logs to be sent to the server.
 */
- (OpaqueObjectRef) generateLogPayloadFor:(InAuthenticateLogSet)logChoices
                                  onError:(NSError **)error __attribute__((swift_error(nonnull_error)));

/**
 Generates an `OpaqueObjectRef` payload containing a user-defined custom log.
 
 @param customLog An `NSDictionary` containing custom data to log.
 @param error The error object used to pass error messages back to caller. The error parameter may be `nil`.
 
 @return An `OpaqueObjectRef` hex byte array containing the user defined custom log to be sent to the server.
 */
- (OpaqueObjectRef) generateCustomLogPayload:(NSDictionary *)customLog
                                     onError:(NSError **)error __attribute__((swift_error(nonnull_error)));

/**
 Generates a `OpaqueObjectRef` payload that sends a request to the server for any pending messages. The server will respond with a registration response `OpaqueObjectRef` which should be passed to the `handlePayload` method to get the list of messages..
 
 @param error The error object used to pass error messages back to the caller. The error parameter may be `nil`.
 
 @return An `OpaqueObjectRef` hex byte array to be sent to the server.
 */
- (OpaqueObjectRef) generatePendingMessagesRequest:(NSError **)error __attribute__((swift_error(nonnull_error)));

/**
 Generates an opaque object that is sent to the server to acknowledge messages.
 
 In the InAuthenticate API the function generateAcknowledgePayload is called by
 acknowledgeServerMessagesOnServer
 
 @param message The message that is being responded to.
 @param response The response given by the customer, often used to accept or deny a message.
 @param error  The error object used to pass error messages back to the caller.
 */
- (OpaqueObjectRef) generateCustomerResponsePayloadFor:(InAuthenticateMessage *)message
                                       response:(NSString *)response
                                        onError:(NSError **)error __attribute__((swift_error(nonnull_error)));

/**
 Generates an opaque object that is sent to the server to acknowledge messages.
 
 In the InAuthenticate API the function generateAcknowledgePayload is called by
 acknowledgeServerMessagesOnServer
 
 @param message The message that is being responded to.
 @param eventId The confirmation identifier for the message list.
 @param priority The priority for the message list.  This value is optional, and may be nil.
 @param response The response given by the customer, often used to accept or deny a message.
 @param error  The error object used to pass error messages back to the caller.
 */
- (OpaqueObjectRef) generateCustomerResponsePayloadFor:(InAuthenticateMessage *) message
                                        eventId:(nullable NSString *) eventId
                                       priority:(nullable NSString *) priority
                                       response:(NSString *) response
                                        onError:(NSError **) error __attribute__((swift_error(nonnull_error)));
    
/**
 Returns a string representation of the LogConfig, Malware, or Root signature list version.
 
 @param type A single `InAuthenticateListType` of either `InAuthenticateListSetRoot`, `InAuthenticateListSetMalware`, or `InAuthenticateListSetTotal`.
 @param error Error object used to pass error messages back to caller. The error parameter may be `nil`.
 
 @return A string representation of the specified list type's version.
 */

- (NSString *) listVersion:(InAuthenticateListSet)type withError:(NSError **)error __attribute__((swift_error(nonnull_error)));

@end
NS_ASSUME_NONNULL_END
