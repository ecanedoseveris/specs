/*
 * Copyright © 2019 InAuth, Inc. All rights reserved.
 * The InAuth logo, and other trademarks, service marks, and designs are registered or unregistered
 * trademarks of InAuth, Inc. and its subsidiaries in the United States and in other countries.
 * All other trademarks are property of their respective owners.
 */

#import <Foundation/Foundation.h>
#import "MMETypes.h"

/**
 Handles communication between the app and the server. This is one possible implementation; custom code may be written for facilitating communication between the iOS SDK and the server.
 */
@interface InMobileNet : NSObject

/**
 Singleton
 */
+ (instancetype _Nonnull )sharedInstance;

/**
 Sends an OpaqueObject to a specific URL.
 This method runs on a background thread and returns the completion handler
 when the network call is complete.
 
 This method replaces sendOpaqueObject:withURL:onCompletion:
 
 The block signature of the onCompletion parameter is the same signature used in
 Apples completion handler for NSURLSession's dataTaskWithRequest:completionHandler: method
 @param opaqueObject The opaque object to be sent to the server.
 @param url The URL of the server that the opaque object is sent to.
 @param complete A completion handler
 */
- (void)sendOpaqueObject:(OpaqueObjectRef _Nonnull )opaqueObject
                   toURL:(NSURL *_Nonnull)url
            onCompletion:(void (^_Nullable)(OpaqueObjectRef _Nullable opaqueObjectResponse,
                                            NSURLResponse * _Nullable urlResponse,
                                            NSError * _Nullable error))complete;

/**
 Sends an OpaqueObject to a specific URL.
 This method runs on a background thread and returns the completion handler
 when the network call is complete.
 @param opaqueObject The opaque object to be sent to the server.
 @param url The URL of the server that the opaque object is sent to.
 @param complete A completion handler
 */
- (void)sendOpaqueObject:(OpaqueObjectRef _Nonnull )opaqueObject
                 withURL:(NSURL *_Nonnull)url
            onCompletion:(void (^_Nullable)(OpaqueObjectRef _Nullable opaqueObjectResponse,
                                   NSError * _Nullable error))complete;

/**
 Sends a get request to a specified server.
 Used for Certificate Pinning and Broadcase Messaging
 
 This method replaces sendGetRequest:withParameters:andCredentials:onError:
 
 This method runs on a background thread and returns the completion handler
 when the network call is complete.
 
 The block signature of the onCompletion parameter is the same signature used in
 Apples completion handler for NSURLSession's dataTaskWithRequest:completionHandler: method
 @param url the url of the server that processes the get request.
 @param complete A completion handler.
 */
- (void)sendGetRequest:(NSURL *_Nonnull)url
          onCompletion:(void (^_Nullable)(OpaqueObjectRef _Nullable opaqueObject,
                                 NSURLResponse * _Nullable urlResponse,
                                 NSError * _Nullable error))complete;

/**
 Sends a get request to a specified server.
 Used for Certificate Pinning and Broadcase Messaging
 
 This method replaces sendGetRequest:withParameters:andCredentials:onError:
 
 This method runs on a background thread and returns the completion handler
 when the network call is complete.
 
 The block signature of the onCompletion parameter is the same signature used in
 Apples completion handler for NSURLSession's dataTaskWithRequest:completionHandler: method
 @param url the url of the server that processes the get request.
 @param parameters Parameters to add to the url.
 @param userCredentials User credentials to add to the http header field.
 */
- (void)sendGetRequest:(NSURL *_Nonnull)url
        withParameters:(NSDictionary *_Nullable)parameters
        andCredentials:(NSString *_Nullable)userCredentials
          onCompletion:(void (^_Nonnull)(OpaqueObjectRef _Nullable opaqueObject,
                                 NSURLResponse * _Nullable response,
                                 NSError * _Nullable error))complete;

/**
 Sends a form to a specified server.
 Used for Broadcast Messaging
 
 This method replaces sendFormURLEncodedPostRequest:withParameters:onError:
 
 This method runs on a background thread and returns the completion handler
 when the network call is complete.
 
 The block signature of the onCompletion parameter is the same signature used in
 Apples completion handler for NSURLSession's dataTaskWithRequest:completionHandler: method
 @param url The url of the server.
 @param parameters Parameters to add to the url.
 @param complete A completion handler
 */
- (void)sendFormURLEncodedPostRequest:(NSURL *_Nonnull)url
                       withParameters:(NSDictionary *_Nonnull)parameters
                         onCompletion:(void (^_Nullable)(OpaqueObjectRef _Nullable opaqueObject,
                                                         NSURLResponse * _Nullable urlResponse,
                                                         NSError * _Nullable error))complete;

/**
 Sends json data to a specified server.
 This replaces the deprecated function sendJSONObject
 @param jsonData  Json data to be sent to the server.
 @param url The url that the json is sent to.
 @param complete  The completion handler.
 */

- (void)sendJSONData:(NSData * _Nonnull )jsonData
               toURL:(NSURL *_Nonnull)url
        onCompletion:(void (^_Nullable)(OpaqueObjectRef _Nullable jsonDataResponse,
                                        NSURLResponse * _Nullable urlResponse,
                                        NSError * _Nullable error))complete;

/**
 Sends an opaque object to a specified server.
 Synchronous.
 @param opaqueObject The opaque object to be sent to the server.
 @param url The URL of the server that the opaque object is sent to.
 @param outError The error message that is returned.
 @return OpaqueObjectRef The opaque object that is returned.
 */
- (OpaqueObjectRef _Nullable )sendOpaqueObject:(OpaqueObjectRef _Nonnull )opaqueObject
                                      toServer:(NSURL *_Nonnull)url
                                       onError:(NSError *_Nullable *_Nullable)outError DEPRECATED_MSG_ATTRIBUTE("use sendOpaqueObject:toURL:onCompletion instead");

/**
 Sends a form to a specified server.
 Used for Broadcast Messaging
 Synchronous
 @param url The url of the server that receives the post request.
 @param parameters Parameters to add to the url.
 @param outError Error returned.
 @return NSData.  The opaque object that is returned from the server.
 */
- (NSData *_Nullable)sendFormURLEncodedPostRequest:(NSURL *_Nonnull)url
                                    withParameters:(NSDictionary *_Nullable)parameters
                                           onError:(NSError *_Nullable *_Nullable)outError DEPRECATED_MSG_ATTRIBUTE("use sendFormURLEncodedPostRequest:withParameters:onCompletion: instead");

/**
 Sends a get request to a specified server.
 Used for Certificate Pinning and Broadcase Messaging
 Synchronous
 @param url  The url of the server that receives the get request.
 @param parameters Parameters to add to the URL
 @param userCredentials User credentials to add to the http header field.
 */
- (NSData *_Nullable)sendGetRequest:(NSURL *_Nonnull)url
                     withParameters:(NSDictionary *_Nullable)parameters
                     andCredentials:(NSString *_Nullable)userCredentials
                            onError:(NSError *_Nullable *_Nullable)outError DEPRECATED_MSG_ATTRIBUTE("use sendGetRequest:withParameters:andCredentials:onCompletion: instead");

/**
 Sends a form to a specified server.
 Used with InAuthenticate and unRegister
 Synchronous
 @param requestData Data object to send to the server.
 @param url  URL of the server that the request is sent to.
 @param payloadFieldName  Name of the payload field.
 @param contentType content Content-type to add to the request body
 @param outError  Error returned.
 
 */
- (NSData *_Nullable)sendFormDataRequest:(NSData * _Nonnull)requestData
                                toServer:(NSURL * _Nonnull)url
                    withPayloadFieldName:(NSString * _Nullable)payloadFieldName
              withRequestBodyContentType:(NSString * _Nullable)contentType
                                 onError:(NSError * _Nullable * _Nullable)outError DEPRECATED_MSG_ATTRIBUTE("For unRegister plese use sendOpaqueObject:toURL:onCompletion: as in the demonstration application instead");

@end
