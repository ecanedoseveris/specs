/*
 * Copyright © 2018 InAuth, Inc. All rights reserved.
 * The InAuth logo, and other trademarks, service marks, and designs are registered or unregistered
 * trademarks of InAuth, Inc. and its subsidiaries in the United States and in other countries.
 * All other trademarks are property of their respective owners.
 */

#import <Foundation/Foundation.h>
#import "InAuthenticateManager.h"
#import "InAuthenticateMessage.h"
#import "InMobile.h"

NS_ASSUME_NONNULL_BEGIN
/**
 Handles completion of enrollment.
 */
typedef void (^onSendComplete)( NSDictionary * _Nullable userInfo,  NSError * _Nullable error);

/**
 Handles completion of message receipt.
 */
typedef void (^onMessagesReceived)( NSArray <InAuthenticateMessage *>* _Nullable messages,  NSError * _Nullable error);

/**
 The InAuthenticate class provides MME class functionality as well as that of the InAuthenticate product. To make use of the InAuthenticate features, you will need an InAuthenticate license.
 */
@interface InAuthenticate : InAuthenticateManager

/**
 Registers the device using the specified registrationURL and enrolls in InAuthenticate. Though the full registration sequence only happens on the first invocation of this method, it is recommended to call `initiateOnServer` each time the app launches. The full registration sequence happens internally as follows: generate registration payload -> send registration payload to server -> handle server's response payload.
 
 @param registrationUrl The URL to send the registration payload to. This URL is also used for `acknowledgeServerMessages`, `getCacheMessages`, `requestListUpdate`, and `sendLogs`.
 @param deviceToken A string representing the deviceToken consumed by Firebase or other APNS system to send messages to the system.
 @param complete A completion handler indicating when initiateOnServer has finished, and whether or not an error occurred.  The completion handler may be nil.
 */
- (void)initiateOnURL:(NSURL *)registrationUrl
          deviceToken:(nullable NSString *)deviceToken
         onCompletion:(onCompletion)complete;
    
- (void)initiateOnServer:(NSURL *)registrationUrl
             deviceToken:(nullable NSString *)deviceToken
            onCompletion:(nullable onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use initiateOnURL instead.");
/**
 Registers the device using the specified registrationURL and enrolls in InAuthenticate. Though the full registration sequence only happens on the first invocation of this method, it is recommended to call `initiateOnServer` each time the app launches. The full registration sequence happens internally as follows: generate registration payload -> send registration payload to server -> handle server's response payload.
 
 @param registrationUrl The URL to send the registration payload to. This URL is also used for `acknowledgeServerMessages`, `getCacheMessages`, `requestListUpdate`, and `sendLogs`.
 @param complete A completion handler indicating when initiateOnServer has finished, and whether or not an error occurred.  The completion handler may be nil.
 */
- (void)initiateOnURL:(NSURL *)registrationUrl
         onCompletion:(onCompletion)complete;

- (void)initiateOnServer:(NSURL *)registrationUrl
            onCompletion:(nullable onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use initiateOnURL instead.");

/**
 Registers the device using the specified registrationURL and enrolls in InAuthenticate. Though the full registration sequence only happens on the first invocation of this method, it is recommended to call `initiateOnServer` each time the app launches. The full registration sequence happens internally as follows: generate registration payload -> send registration payload to server -> handle server's response payload.
 
 @param registrationUrl The URL to send the registration payload to. This URL is also used for `acknowledgeServerMessages`, `getCacheMessages`, `requestListUpdate`, and `sendLogs`.
 @param deviceToken A string representing the deviceToken consumed by Firebase or other APNS system to send messages to the system.
 @param customMap A map that can be used to pass custom data to the enrollment server.
 @param complete A completion handler indicating when initiateOnServer has finished, and whether or not an error occurred.  The completion handler may be nil.
 */
- (void)initiateOnURL:(NSURL *)registrationUrl
          deviceToken:(nullable NSString *)deviceToken
            customMap:(nullable NSDictionary *)customMap
         onCompletion:(onCompletion)complete;

- (void)initiateOnServer:(NSURL *)registrationUrl
             deviceToken:(nullable NSString *)deviceToken
               customMap:(nullable NSDictionary *)customMap
            onCompletion:(nullable onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use initiateOnURL instead.");

/**
 Registers the device using the specified registrationURL and enrolls in InAuthenticate. Though the full registration sequence only happens on the first invocation of this method, it is recommended to call `initiateOnServer` each time the app launches. The full registration sequence happens internally as follows: generate registration payload -> send registration payload to server -> handle server's response payload.
 
 @param registrationUrl The URL to send the registration payload to. This URL is also used for `acknowledgeServerMessages`, `getCacheMessages`, `requestListUpdate`, and `sendLogs`.
 @param customMap A map that can be used to pass custom data to the enrollment server.
 @param complete A completion handler indicating when initiateOnServer has finished, and whether or not an error occurred.  The completion handler may be nil.
 */
- (void)initiateOnURL:(NSURL *)registrationUrl
            customMap:(nullable NSDictionary *)customMap
         onCompletion:(onCompletion)complete;

- (void)initiateOnServer:(NSURL *)registrationUrl
               customMap:(nullable NSDictionary *)customMap
            onCompletion:(nullable onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use initiateOnURL instead.");
/**
 Generates and sends the selected logs, an optional custom log, and an optional transaction ID to the specified URL.
 
 @param url The url for the log server.  This URL usually has the same host as the registration server, although the path may be different.
 @param logSet An `InMobileLogSet` type of any or all of the available log choices. Valid options are `GPS`, `MALWARE`, `ROOT`.
 @param customLog An `NSDictionary` containing custom data to log. The customLog parameter may be `nil`.
 @param transactionId Provides a mechanism to group multiple log transmissions into a customer-defined transaction. Can be set to any string of length 0 to 255. The transactionId parameter may be `nil`.
 @param complete A completion handler indicating when `sendLogs` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)sendInAuthenticateLogs:(InAuthenticateLogSet)logSet
                     customLog:(nullable NSDictionary *)customLog
                 transactionId:(nullable NSString *)transactionId
                         toURL:(NSURL *)url
                  onCompletion:(onCompletion)complete;
    
- (void)sendLogsToServer:(NSURL *)url
                  logSet:(InAuthenticateLogSet)logSet
               customLog:(nullable NSDictionary *)customLog
           transactionId:(nullable NSString *)transactionId
            onCompletion:(nullable onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use sendInAuthenticateLogs instead.");

/** A convenience function that calls sendLogs.
 @param url The URL to send the log payload to. The url parameter may be `nil`, in which case the URL passed into `initiate` will be used as a default URL.
 @param complete A completion handler indicating when `sendLogs` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)sendInAuthenticateLogsToURL:(NSURL *)url
                       onCompletion:(onCompletion)complete;

- (void)sendLogsToServer:(NSURL *)url
            onCompletion:(nullable onCompletion)complete  DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use sendInAuthenticateLogs instead.");

/** A convenience function that calls the general version of sendLogs.
 @param url The URL to send the log payload to. The url parameter may be `nil`, in which case the URL passed into `initiate` will be used as a default URL.
 @param transactionId Provides a mechanism to group multiple log transmissions into a customer-defined transaction. Can be set to any string of length 0 to 255. The transId parameter may be `nil`.
 @param complete A completion handler indicating when `sendLogs` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)sendInAuthenticateLogs:(NSURL *)url
                 transactionId:(nullable NSString *)transactionId
                  onCompletion:(onCompletion)complete;

- (void)sendLogsToServer:(NSURL *)url
           transactionId:(nullable NSString *)transactionId
            onCompletion:(nullable onCompletion)complete  DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use sendInAuthenticateLogs instead.");

/** A convenience function that calls the general version of sendLogs.
 @param url The URL to send the log payload to. The url parameter may be `nil`, in which case the URL passed into `initiate` will be used as a default URL.
 @param logSet An `InMobileLogSet` type of any or all of the available log choices. Valid options are `GPS`, `MALWARE`, `ROOT`.
 @param transactionId Provides a mechanism to group multiple log transmissions into a customer-defined transaction. Can be set to any string of length 0 to 255. The transId parameter may be `nil`.
 @param complete A completion handler indicating when `sendLogs` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)sendInAuthenticateLogs:(InAuthenticateLogSet)logSet
                 transactionId:(NSString *)transactionId
                         toURL:(NSURL *)url
                  onCompletion:(onCompletion)complete;

- (void)sendLogsToServer:(NSURL *)url
                  logSet:(InAuthenticateLogSet)logSet
           transactionId:(NSString *)transactionId
            onCompletion:(nullable onCompletion)complete  DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use sendInAuthenticateLogs instead.");

/**
 Fetches any messages stored on the Risk Server that have been queued up by the app server. This should be invoked each time a push notification is received.
 
 @param url The url for the message server.  This URL usually has the same host as the registration server, although the path may be different.
 @param complete A completion block that returns an array of InAuthenticateMessage objects that can be parsed (see InAuthenticateMessage.h) and displayed to the user to respond to.
 */
- (void)getPendingMessagesFromURL:(NSURL *)url
                     onCompletion:(onMessagesReceived)complete;
    
- (void)getPendingMessagesFromServer:(NSURL *)url
                          onComplete:(nullable onMessagesReceived)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use getPendingMessagesFromURL instead.");


/**
 Sends a user response to the server. This also allows queued up messages to be destroyed.
 
 @param url The url of the server that receives acknowledgements.  This URL usually has the same host as the registration server, although the path may be different.
 @param message The message that is being responded to.
 @param response The response given by the customer, often used to accept or deny a message.
 @param complete A completion block that returns a userInfo dictionary containing whether or not the server was able to acknowledge the messages and an error if one occurred.
 */
- (void)sendCustomerResponse:(NSString *)response
                  forMessage:(InAuthenticateMessage *)message
                       toURL:(NSURL *)url
                onCompletion:(onSendComplete)complete;

- (void)sendCustomerResponseToServer:(NSURL *)url
                          forMessage:(InAuthenticateMessage *)message
                                with:(NSString *)response
                          onComplete:(nullable onSendComplete)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use sendCustomerResponse:onCompletion instead.");

/**
 Sends a user response to the server. This also allows queued up messages to be destroyed. Also includes eventId & priority fields
 
 @param url The url of the server that receives acknowledgements.  This URL usually has the same host as the registration server, although the path may be different.
 @param message The message that is being responded to.
 @param response The response given by the customer, often used to accept or deny a message.
 @param complete A completion block that returns a userInfo dictionary containing whether or not the server was able to acknowledge the messages and an error if one occurred.
 */
- (void)sendCustomerResponse:(NSString *)response
                  forMessage:(InAuthenticateMessage *)message
                     eventId:(nullable NSString *) eventId
                    priority:(nullable NSString *) priority
                       toURL:(NSURL *)url
                onCompletion:(onSendComplete)complete;
    
- (void)sendCustomerResponseToServer:(NSURL *)url
                          forMessage:(InAuthenticateMessage *)message
                             eventId:(nullable NSString *) eventId
                            priority:(nullable NSString *) priority
                                with:(NSString *)response
                          onComplete:(nullable onSendComplete)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use sendCustomerResponse:onCompletion instead.");

/**
 Update the GCM token.  This should be called whenever firebase refreshes the token.
 
 @param url  The url of the server where the token is refreshed.  This URL usually has the same host as the enrollment server, although the path may be different.
 @param deviceToken The google cloud message token, also known as the firebase token.
 @param complete A completion block that returns a userInfo dictionary containing the response from the server and an error if one occurred.
 */
    
- (void)updateDeviceTokenOnServer:(NSURL *)url
                      deviceToken:(nullable NSString *)deviceToken
                     onCompletion:(nullable onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. This api is unnecessary initiateOnURL will provide this functionality.");

/**
 Returns a string representation of the Malware, Root or both signature list version.
 
 @param type A single `InAuthenticateListType` of either `InAuthenticateRootList`, `InAuthenticateMalwareList`, or `InAuthenticateTotalListSet`.
 @param error Error object used to pass error messages back to caller. The error parameter may be `nil`.
 
 @return A string representation of the specified list type's version.
 */

- (nullable NSString *) requestListVersion:(InAuthenticateListType)type
                                   onError:(NSError **)error DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use requestListVersion in InAuthenticateManager instead.");

/**
 Downloads and installs a set of signature files to the device. The user info dictionary returned upon a successful list update contains, if requested, the root and malware version, and log config `InMobileLogSet` value.
 
 @param url  The url of the server that provides list updates.  This URL usally has the same host as the registration server, although the path may be different.
 @param selection An `InAuthenticateListSet` of any combination of `InAuthenticateRootList` and `InAuthenticateMalwareList`.
 @param complete A completion handler indicating when `updateList` has finished, whether or not an error occurred, and a dictionary containing user info pertaining to the updated signature file(s). The complete parameter may be `nil`.
 */
    
- (void)updateInAuthenticateList:(InAuthenticateListSet)selection
                         fromUrl:(NSURL *)url
                    onCompletion:(listCompletion)complete;

- (void)requestListUpdateFromServer:(NSURL *) url
                          selection:(InAuthenticateListSet)selection
                         onComplete:(nullable onSendComplete)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use updateInAuthenticateList:fromURL:onComplete instead.");



@end
NS_ASSUME_NONNULL_END
