/*
 * Copyright © 2018 InAuth, Inc. All rights reserved.
 * The InAuth logo, and other trademarks, service marks, and designs are registered or unregistered
 * trademarks of InAuth, Inc. and its subsidiaries in the United States and in other countries.
 * All other trademarks are property of their respective owners.
 */

#import "MME.h"

NS_ASSUME_NONNULL_BEGIN
/**
 Handles completion of methods that return nothing or an error.
 */
typedef void (^onCompletion)(NSError * _Nullable error);

/**
 Handles completion of list updating.
 */
typedef void (^listCompletion)(NSDictionary *_Nullable userInfo, NSError *_Nullable error);

/**
 MMEController is a Swift-friendly class that provides functionality of the MME class as well as several methods designed as a more concise interface for typical use cases.
 
 MMEController methods that make use of networking do so asynchronously.
 */
@interface MMEController : MME

/**
 Registers the device using the specified URL. Though the full registration sequence only happens on the first invocation of this method, it is recommended to call `initiate` each time the app launches. The full registration sequence happens internally as follows: generate registration payload -> send registration payload to server -> handle server's response payload.

 @param url The URL to send the registration payload to (e.g. an app server endpoint).
 @param complete A completion handler indicating when `initiate` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)initiateOnURL:(NSURL *)url onCompletion:(onCompletion)complete;

/**
 Registers the device using the specified URL. Though the full registration sequence only happens on the first invocation of this method, it is recommended to call `initiate` each time the app launches. The full registration sequence happens internally as follows: generate registration payload -> send registration payload to server -> handle server's response payload.
 
 @param url The URL to send the registration payload to (e.g. an app server endpoint).
 @param complete A completion handler indicating when `initiate` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)initiateOnURL:(NSURL *)url
            customLog:(NSDictionary * _Nullable)customLog
         onCompletion:(onCompletion)complete;

/**
 Generates and sends the selected logs, an optional custom log, and an optional transaction ID to the specified URL.
 
 @param logChoices An `InMobileLogSet` type of any or all of the available log choices.
 @param customLog An `NSDictionary` containing custom data to log. The customLog parameter may be `nil`.
 @param transId Provides a mechanism to group multiple log transmissions into a customer-defined transaction. Can be set to any string of length 0 to 255. The transId parameter may be `nil`.
 @param url The URL to send the log payload to.
 @param complete A completion handler indicating when `sendLogs` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)sendLogs:(InMobileLogSet)logChoices
       customLog:(NSDictionary * _Nullable)customLog
   transactionId:(NSString * _Nullable)transId
           toUrl:(NSURL *)url
    onCompletion:(onCompletion)complete;

/**
 Generates and sends a custom log and an optional transaction ID to the specified URL.
 
 @param customLog An `NSDictionary` containing custom data to log.
 @param transId Provides a mechanism to group multiple log transmissions into a customer-defined transaction. Can be set to any string of length 0 to 255. The transId parameter may be `nil`.
 @param url The URL to send the log payload to.
 @param complete A completion handler indicating when `sendLogs` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)sendCustomLogs:(NSDictionary*)customLog
         transactionId:(NSString * _Nullable)transId
                 toUrl:(NSURL *)url
          onCompletion:(onCompletion)complete;

/**
 Downloads and installs a set of signature files to the device. The user info dictionary returned upon a successful list update contains, if requested, the root and malware version, and log config `MMELogSet` value.
 
 @param selection An `InMobileListSet` type of any or all of the available log choices..
 @param url The URL to request the signature files from.
 @param complete A completion handler indicating when `updateList` has finished, whether or not an error occurred, and a dictionary containing user info pertaining to the updated signature file(s). The complete parameter may be `nil`.
 */
- (void)updateList:(InMobileListSet)selection
           fromUrl:(NSURL *)url
      onCompletion:(listCompletion)complete;

/**
 Generates and sends a binding `OpaqueObjectRef` to the specified server. Once the device is bound, the `bindBrowserOpen` method must be invoked to complete the binding process.
 
 @param url The URL to send the binding object to.
 @param complete A completion handler indicating when `bindBrowser` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)bindBrowserToUrl:(NSURL *)url onCompletion:(onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.1");

@end
NS_ASSUME_NONNULL_END
