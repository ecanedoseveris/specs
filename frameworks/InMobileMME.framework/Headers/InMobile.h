/*
 * Copyright © 2018 InAuth, Inc. All rights reserved.
 * The InAuth logo, and other trademarks, service marks, and designs are registered or unregistered
 * trademarks of InAuth, Inc. and its subsidiaries in the United States and in other countries.
 * All other trademarks are property of their respective owners.
 
 * This file is deprecated as of 9.0.0. Available for backwards compatibility

 */

#import "MMEController.h"

/**
 The InMobile class provides the functionality of the MME class as well as several methods designed as a more concise interface for typical use cases.
 
 InMobile methods that make use of networking do so asynchronously.
 */
@interface InMobile : MMEController

/**
 Registers the device using the specified URL. Though the full registration sequence only happens on the first invocation of this method, it is recommended to call `initiate` each time the app launches. The full registration sequence happens internally as follows: generate registration payload -> send registration payload to server -> handle server's response payload.
 ```
 [inMobileMngr initiate:url onCompletion:^(NSError *error) {
    if (error) {
        NSLog(@"initiate Error: %@", error);
    } else {
        // Use SDK
    }
 }];
 ```
 
 @param url The URL to send the registration payload to (e.g. an app server endpoint). This URL is also used as a fallback URL if no URL is passed into `sendLogs`, `updateList`, or `bindBrowser`.
 @param complete A completion handler indicating when `initiate` has finished, and whether or not an error occurred. The complete parameter may be `nil`. 
 */
- (void)initiate:(NSURL *)url onCompletion:(onCompletion)complete  DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use the MMEController class instead");

/**
 Registers the device using the specified URL. Though the full registration sequence only happens on the first invocation of this method, it is recommended to call `initiate` each time the app launches. The full registration sequence happens internally as follows: generate registration payload -> send registration payload to server -> handle server's response payload.
 ```
 [inMobileMngr initiate:url onCompletion:^(NSError *error) {
 if (error) {
 NSLog(@"initiate Error: %@", error);
 } else {
 // Use SDK
 }
 }];
 ```
 
 @param url The URL to send the registration payload to (e.g. an app server endpoint). This URL is also used as a fallback URL if no URL is passed into `sendLogs`, `updateList`, or `bindBrowser`.
 @param complete A completion handler indicating when `initiate` has finished, and whether or not an error occurred. The complete parameter may be `nil`.
 */
- (void)initiate:(NSURL *)url
       customLog:(NSDictionary *)customLog
    onCompletion:(onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use the MMEController class instead");

/**
 Generates and sends the selected logs, an optional custom log, and an optional transaction ID to the specified URL.
 
 ```
 // Using completion handler
 [inMobileMngr sendLogs:DEVICE customLog:nil transactionId:nil url:url onCompletion:^(NSError *error) {
    if (error) {
        NSLog(@"sendLogs Error: %@", error.localizedDescription);
    }
 }];
 
 // Send and forget
 [inMobileMngr sendLogs:DEVICE customLog:nil transactionId:nil url:url onCompletion:nil];
 ```
 
 @param logChoices An `MMELogSet` type of any or all of the available log choices. Valid options are `ACCELEROMETER`, `ANOMALIES`,`APPLICATION_INFO`, `BATTERY`, `BLUETOOTH`, `CAMERA`, `CONTACT`, `CALENDAR`, `DATA_USAGE`, `DEVICE`, `DEVICE_ACCESS`, `GPS`, `HARDWARE`, `LOCALE`, :`MALWARE`, `MEDIA`, `ROOT`, `SCREEN`, `TELEPHONE`, `WIFI`, `WHITEBOX`, and `TOTAL_LOG_SET`.
 @param customLog An `NSDictionary` containing custom data to log. The customLog parameter may be `nil`.
 @param transId Provides a mechanism to group multiple log transmissions into a customer-defined transaction. Can be set to any string of length 0 to 255. The transId parameter may be `nil`.
 @param url The URL to send the log payload to. The url parameter may be `nil`, in which case the URL passed into `initiate` will be used as a default URL.
 @param complete A completion handler indicating when `sendLogs` has finished, and whether or not an error occurred. The complete parameter may be `nil`. 
 */
- (void)sendLogs:(MMELogSet)logChoices
       customLog:(NSDictionary *)customLog
   transactionId:(NSString *)transId
             url:(NSURL *)url
    onCompletion:(onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use the MMEController class instead");

/**
 Generates and sends a custom log and an optional transaction ID to the specified URL.
 
 ```
 NSDictionary *customLog = @{@"custom" : @"log"};
 // Using completion handler
 [inMobileMngr sendLogs:customLog transactionId:nil url:url onCompletion:^(NSError *error) {
    if (error) {
        NSLog(@"sendLogs Error: %@", error.localizedDescription);
    }
 }];
 
 // Send and forget
 [inMobileMngr sendLogs:customLog transactionId:nil url:url onCompletion:nil];
 ```
 
 @param customLog An `NSDictionary` containing custom data to log. The customLog parameter may be `nil`.
 @param transId Provides a mechanism to group multiple log transmissions into a customer-defined transaction. Can be set to any string of length 0 to 255. The transId parameter may be `nil`.
 @param url The URL to send the log payload to. The url parameter may be `nil`, in which case the URL passed into `initiate` will be used as a default URL.
 @param complete A completion handler indicating when `sendLogs` has finished, and whether or not an error occurred. The complete parameter may be `nil`. 
 */
- (void)sendLogs:(NSDictionary*)customLog
   transactionId:(NSString *)transId
             url:(NSURL *)url
    onCompletion:(onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use the MMEController class instead");

/**
 Downloads and installs a set of signature files to the device. The user info dictionary returned upon a successful list update contains, if requested, the root and malware version, and log config `MMELogSet` value.
 
 ```
 // Using completion handler
 [inMobileMngr updateList:MMETotalListSet url:url onCompletion:^(NSDictionary *userInfo, NSError *error) {
    if (error) {
        NSLog(@"updateList Error: %@", error);
    } else {
        NSLog(@"updateList User Info: %@", userInfo);
    }
 }];
 
 // Update and forget
 [inMobileMngr updateList:MMETotalListSet url:url onCompletion:nil];
 ```
 
 @param selection An `MMEListSet` of any combination of `MMERootList`, `MMEMalwareList`, `MMELogConfig`, or `MMETotalListSet`. 
 @param url The URL to request the signature files from. The url parameter may be `nil`, in which case the URL passed into `initiate` will be used as a default URL.
 @param complete A completion handler indicating when `updateList` has finished, whether or not an error occurred, and a dictionary containing user info pertaining to the updated signature file(s). The complete parameter may be `nil`. 
 */
- (void)updateList:(MMEListSet)selection
               url:(NSURL *)url
      onCompletion:(listCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use the MMEController class instead");

/**
 Generates and sends a binding `OpaqueObjectRef` to the specified server. Once the device is bound, the `bindBrowserOpen` method must be invoked to complete the binding process.
 
 ```
 [inMobileMngr bindBrowser:url onCompletion:^(NSError *error) {
    if (error) {
        NSLog(@"bindBrowser Error: %@", error);
    } else {
        [inMobileMngr bindBrowserOpen:bindingURL onError:nil];
    }
 }];
 ```
 
 @param url The URL to send the binding object to. The url parameter may be `nil`, in which case the URL passed into `initiate` will be used as a default URL.
 @param complete A completion handler indicating when `bindBrowser` has finished, and whether or not an error occurred. The complete parameter may be `nil`. 
 */
- (void)bindBrowser:(NSURL *)url onCompletion:(onCompletion)complete DEPRECATED_MSG_ATTRIBUTE("as of v9.0. Use the MMEController class instead");

@end
