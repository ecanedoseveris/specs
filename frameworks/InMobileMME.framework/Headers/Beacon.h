//
//  Beacon.h
//  IACoreProducts
//
//  Created by Waleed Rahman on 2/13/20.
//  Copyright © 2020 InAuth. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMEController.h"

NS_ASSUME_NONNULL_BEGIN

@class Beacon;

@interface MMEController (Beacon)

/**
    Returns an instance of Beacon Manager used to log and send information to Beacon server. There should only be one instance of Beacon.
 
    @param error Error object used to pass error messages back to caller.
 
    @return A `Beacon` manager object used to log information about user activity..
*/
-(nullable Beacon *) sharedBeaconInstanceWithError:(NSError **)error;

@end


@interface Beacon : NSObject

/**
 The session ID set.
 */
@property (atomic, readwrite, strong, nullable) NSString *sessionID;

/**
 The internally generated beacon session id. Readonly
*/
@property (atomic, readonly, strong) NSString *beaconSessionID;

/**
    Logs a ScreenStart event with a custom, unique screen identifier and screen title.
 
    @param screenID A unique ID to identify the screen being viewed.
    @param title A human-readable string to identify the screen title.
 */
- (void)screenStartEventFor:(NSInteger)screenID title:(NSString *)title;

/**
   Logs a ScreenEnd event with a custom, unique screen identifier and screen title.

   @param screenID A unique ID to identify the screen being viewed. It should be the same ID that was sent with ScreenStart event.
*/
- (void)screenEndEventFor:(NSInteger)screenID;

/**
    Uploads all beacon logs from the application to the Beacon server. This is not necesary to be called as data will automatically be sent periodically.
*/
- (void)upload;

/**
    Generates a large random number.
*/
- (NSInteger)randomLarge;

@end

NS_ASSUME_NONNULL_END
