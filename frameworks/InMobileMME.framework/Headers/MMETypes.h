/*
 * Copyright © 2018 InAuth, Inc. All rights reserved.
 * The InAuth logo, and other trademarks, service marks, and designs are registered or unregistered
 * trademarks of InAuth, Inc. and its subsidiaries in the United States and in other countries.
 * All other trademarks are property of their respective owners.

 * This file is deprecated as of 9.0.0. Available for backwards compatibility

 */

/** @file MMETypes.h */
#import <Foundation/Foundation.h>
#import "InMobileTypes.h"

NS_ASSUME_NONNULL_BEGIN

/// A set of logs to be gathered by the SDK.
typedef UInt64 MMELogSet __deprecated_msg("as of v9.0: Use InMobileLogSet instead.");
/// Available MMELogSet options.
typedef NS_OPTIONS(UInt64, MMELogOptions) {
    EMPTY_LOG_SET       __deprecated_enum_msg("as of v9.0: Use InMobileLogSetEmpty instead.")           = InMobileLogSetEmpty,
    ACCELEROMETER       __deprecated_enum_msg("as of v9.0: Use InMobileLogSetAccelerometer instead.")   = InMobileLogSetAccelerometer,
    ANOMALIES           __deprecated_enum_msg("as of v9.0: Use InMobileLogSetAnomalies instead.")       = InMobileLogSetAnomalies,
    APPLICATION_INFO    __deprecated_enum_msg("as of v9.0: Use InMobileLogSetApplicationInfo instead.") = InMobileLogSetApplicationInfo,
    BATTERY             __deprecated_enum_msg("as of v9.0: Use InMobileLogSetBattery instead.")         = InMobileLogSetBattery,
    BLUETOOTH           __deprecated_enum_msg("as of v9.0: Use InMobileLogSetBluetooth instead.")       = InMobileLogSetBluetooth,
    CAMERA              __deprecated_enum_msg("as of v9.0: Use InMobileLogSetCamera instead.")          = InMobileLogSetCamera,
    CONTACT             __deprecated_enum_msg("as of v9.0: Use InMobileLogSetContact instead.")         = InMobileLogSetContact,
    DATA_USAGE          __deprecated_enum_msg("as of v9.0: Use InMobileLogSetDataUsage instead.")       = InMobileLogSetDataUsage,
    DEVICE              __deprecated_enum_msg("as of v9.0: Use InMobileLogSetDevice instead.")          = InMobileLogSetDevice,
    DEVICE_ACCESS       __deprecated_enum_msg("as of v9.0: Use InMobileLogSetDeviceAccess instead.")    = InMobileLogSetDeviceAccess,
    GPS                 __deprecated_enum_msg("as of v9.0: Use InMobileLogSetGPS instead.")             = InMobileLogSetGPS,
    HARDWARE            __deprecated_enum_msg("as of v9.0: Use InMobileLogSetHardware instead.")        = InMobileLogSetHardware,
    LOCALE              __deprecated_enum_msg("as of v9.0: Use InMobileLogSetLocale instead.")          = InMobileLogSetLocale,
    MALWARE             __deprecated_enum_msg("as of v9.0: Use InMobileLogSetMalware instead.")         = InMobileLogSetMalware,
    MEDIA               __deprecated_enum_msg("as of v9.0: Use InMobileLogSetMedia instead.")           = InMobileLogSetMedia,
    PERMISSIONS         __deprecated_enum_msg("as of v9.0: Use InMobileLogSetPermissions instead.")     = InMobileLogSetPermissions,
    ROOT                __deprecated_enum_msg("as of v9.0: Use InMobileLogSetRoot instead.")            = InMobileLogSetRoot,
    SCREEN              __deprecated_enum_msg("as of v9.0: Use InMobileLogSetScreen instead.")          = InMobileLogSetScreen,
    TELEPHONE           __deprecated_enum_msg("as of v9.0: Use InMobileLogSetTelephone instead.")       = InMobileLogSetTelephone,
    WIFI                __deprecated_enum_msg("as of v9.0: Use InMobileLogSetWifi instead.")            = InMobileLogSetWifi,
    WHITEBOX            __deprecated_enum_msg("as of v9.0: Use InMobileLogSetWhitebox instead.")        = InMobileLogSetWhitebox,
    IPA_DIGEST          __deprecated_enum_msg("as of v9.0: Use InMobileLogSetIpaDigest instead.")       = InMobileLogSetIpaDigest,
    SERVER_LOG_CONFIG   __deprecated_enum_msg("as of v9.0: Use InMobileLogSetServerLog instead.")       = InMobileLogSetServerLog,
    TOTAL_LOG_SET       __deprecated_enum_msg("as of v9.0: Use InMobileLogSetAll instead.")             = InMobileLogSetAll,
    INAUTHENTICATE_LOG_SET __deprecated_enum_msg("as of v9.0: Use InAuthenticateLogSetAll instead.")    = InMobileLogSetGPS | InMobileLogSetRoot | InMobileLogSetMalware
};

/// Specifies which types of lists should be updated.
typedef UInt16 MMEListSet __deprecated_msg("as of v9.0: Use InMobileListSet instead.");
/// Available MMEListSet options.
typedef NS_ENUM(UInt16, MMEListType) {
    MMEEmptyListSet         __deprecated_enum_msg("as of v9.0: Use InMobileListSetEmpty instead.")           = InMobileListSetEmpty,
    MMERootList             __deprecated_enum_msg("as of v9.0: Use InMobileListSetRoot instead.")            = InMobileListSetRoot,
    MMEMalwareList          __deprecated_enum_msg("as of v9.0: Use InMobileListSetMalware instead.")         = InMobileListSetMalware,
    MMELogConfig            __deprecated_enum_msg("as of v9.0: Use InMobileListSetLogConfig instead.")       = InMobileListSetLogConfig,
    MMERemoteRootList       __deprecated_enum_msg("as of v9.0: Use InMobileListSetRemoteRoot instead.")      = InMobileListSetRemoteRoot,
    MMERemoteMalwareList    __deprecated_enum_msg("as of v9.0: Use InMobileListSetRemoteMalware instead.")   = InMobileListSetRemoteMalware,
    MMERemoteLogConfig      __deprecated_enum_msg("as of v9.0: Use InMobileListSetRemoteLogConfig instead.") = InMobileListSetRemoteLogConfig,
    MMETotalListSet         __deprecated_enum_msg("as of v9.0: Use InMobileListSetAll instead.")             = InMobileListSetAll
} __deprecated_msg("as of v9.0: Use InMobileListSet instead.");

/// Available InAuthenticateListSet options.
typedef NS_OPTIONS(UInt16, InAuthenticateListType) {
    InAuthenticateEmptyListSet     = InAuthenticateListSetEmpty,
    InAuthenticateRootList         = InAuthenticateListSetRoot,
    InAuthenticateMalwareList      = InAuthenticateListSetMalware,
    InAuthenticateTotalListSet     = InAuthenticateListSetTotal
} __deprecated_msg("as of v9.0: Use InAuthenticateListSet instead.");

/// Denotes a type of storage.
typedef UInt8 StorageType  __attribute__((deprecated("as of 9.0")));
/// Available StorageType options.
typedef NS_OPTIONS(UInt8, StorageTypes) {
    Undefined = 0,
    SSE,
    HSE
}  __attribute__((deprecated("as of 9.0")));

/// Denotes a type of policy to be used in whitebox storage.
typedef UInt16 MMEWhiteBoxPolicySet __deprecated_msg("Use InMobileWhiteBoxPolicySet instead");
/// Available MMEWhiteBoxPolicySet options.
typedef NS_OPTIONS(UInt16, MMEWhiteBoxPolicyOptions) {
    ROOTED_POLICY     = InMobileWhiteBoxPolicyRoot,
    MALWARE_POLICY    = InMobileWhiteBoxPolicyMalware,
    LOCATION_POLICY   = InMobileWhiteBoxPolicyLocation,
    WIFI_POLICY       = InMobileWhiteBoxPolicyWifi,
    ROOT_CLOAK_POLICY = InMobileWhiteBoxPolicyRootCloak
} __deprecated_msg("Use InMobileWhiteBoxPolicySet instead");

/// Denotes the state of a device in relation to several considerations, such as device rooting and the presence of malware.
typedef SInt16 MMEState __deprecated_msg("Use InMobileMalwareState, InMobileRootState or InMobileDeviceState depending on your use case instead");
/// Available MMEState options.
typedef NS_OPTIONS(SInt16, MMEStates){
    MMETrue                         = 1,
    MMEFalse                        = -1,
    MMEError                        = (1u << 1), /**< 2 */
    MMERooted                       = (1u << 2), /**< 4 */
    MMENotRooted                    = ~(MMERooted), /**< -5 */
    MMEMalwareDetected              = (1u << 3), /**< 8 */
    MMECompromised                  = (1u << 4), /**< 16 */
    MMENoMalwareDetected            = ~(MMEMalwareDetected), /**< -9 */
    MMERootedAndCloaked             = (1u << 5), /**< 32 */
    MMENotCloaked                   = ~(MMERootedAndCloaked),
    MMENotRootedNoMalwareDetected   = (1u << 6), /**< 64 */
    MMEUndefined                    = (1u << 8)  /**< 256 */
} __deprecated_msg("Use InMobileMalwareState, InMobileRootState or InMobileDeviceState depending on your use case instead");

typedef MMEState InAuthState __deprecated_msg("Use InMobileMalwareState, InMobileRootState or InMobileDeviceState depending on your use case instead");
typedef MMEStates InAuthStates __deprecated_msg("Use InMobileMalwareState, InMobileRootState or InMobileDeviceState depending on your use case instead");

/// Denotes a type of user authentication.
typedef UInt16 AuthType __deprecated_msg("as of v9.0: Use InMobileAuthType instead.");
/// Available AuthType options.
typedef NS_OPTIONS(UInt16, Authentication) {
    BIOMETRICS              __deprecated_enum_msg("as of v9.0: Use InMobileAuthTypeBiometrics instead.")         = InMobileAuthTypeBiometrics,
    BIOMETRICS_PASSCODE     __deprecated_enum_msg("as of v9.0: Use InMobileAuthTypeBiometricsPasscode instead.") = InMobileAuthTypeBiometricsPasscode,
    FINGERPRINT             __deprecated_enum_msg("as of v9.0: Use InMobileAuthTypeBiometrics instead.")         = InMobileAuthTypeBiometrics,
    FINGERPRINT_PASSCODE    __deprecated_enum_msg("as of v9.0: Use InMobileAuthTypeBiometricsPasscode instead.") = InMobileAuthTypeBiometricsPasscode
} __deprecated_msg("as of v9.0: Use InMobileAuthType instead.");

/// Denotes the state of the device in regards to user authentication.
typedef NSInteger AuthState  __deprecated_msg("as of v9.0: Use InMobileAuthError instead.");
/// Available AuthState options.
typedef NS_OPTIONS(NSInteger, AuthStates){
    AUTHENTICATION_FAILED       __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorFailed instead.")               = InMobileAuthErrorFailed,
    UNSUPPORTED_BIOMETRIC       __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorUnsupported instead.")          = InMobileAuthErrorUnsupported,
    UNRECOGNIZED_TYPE           __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorUnrecognizedType instead.")     = InMobileAuthErrorUnrecognizedType,
    UNSUPPORTED_OS_VERSION      __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorUnsupportedOsVersion instead.") = InMobileAuthErrorUnsupportedOsVersion,
    UNSUPPORTED_OS_FEATURE      __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorUnsupportedOsFeature instead.") = InMobileAuthErrorUnsupportedOsFeature,
    FALLBACK                    __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorFallback instead.")             = InMobileAuthErrorFallback,
    AUTHENTICATION_CANCELED     __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorCanceled instead.")             = InMobileAuthErrorCanceled,
    PASSCODE_NOT_SET            __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorPasscodeNotSet instead.")       = InMobileAuthErrorPasscodeNotSet,
    NOT_ENROLLED_BIOMETRIC      __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorNotEnrolled instead.")          = InMobileAuthErrorNotEnrolled,
    AUTHENTICATION_ERROR        __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorUnknown instead.")              = InMobileAuthErrorUnknown,
    INVALID_AUTH_STATE          __deprecated_enum_msg("as of v9.0: Use InMobileAuthErrorInvalid instead.")              = InMobileAuthErrorInvalid,
} __deprecated_msg("as of v9.0: Use InMobileAuthError instead.");

/// Denotes a licensing issue.
typedef UInt64 INMOBILE_SDK_LICENSE __deprecated_msg("as of v9.0: Use InMobileSDKLicense instead.");
/// Available INMOBILE_SDK_LICENSE options.
typedef NS_OPTIONS(UInt64, LICENSE) {
    LICENSE_NOT_FOUND               __deprecated_enum_msg("as of v9.0: Use InMobileSDKLicenseNotFound instead.")                     = InMobileSDKLicenseNotFound,
    EXPIRED                         __deprecated_enum_msg("as of v8.8.0.")                                                           = InMobileSDKLicenseExpired,
    BIOMETRICS_NOT_PROVISIONED      __deprecated_enum_msg("as of v9.0: Use InMobileSDKLicenseBiometricsNotProvisioned instead.")     = InMobileSDKLicenseBiometricsNotProvisioned,
    INMOBILE_NOT_PROVISIONED        __deprecated_enum_msg("as of v9.0: Use InMobileSDKLicenseInMobileNotProvisioned instead.")       = InMobileSDKLicenseInMobileNotProvisioned,
    INAUTHENTICATE_NOT_PROVISIONED  __deprecated_enum_msg("as of v9.0: Use InMobileSDKLicenseInAuthenticateNotProvisioned instead.") = InMobileSDKLicenseInAuthenticateNotProvisioned
} __deprecated_msg("as of v9.0: Use InMobileSDKLicense instead.");

/**
 The MMETypes class encapsulates several class methods useful for converting strings into types relevant to the iOS SDK.
 */
@interface MMETypes : NSObject

/**
 * Converts an MMEState to its NSString representation
 * @param state An MMEState value.
 * @return NSString of the requested state value.
 **/
+ (NSString *)stateToNSString:(MMEState)state __deprecated_msg("as of v9.0. Use InMobileTypes.malwareStateToString, InMobileTypes.rootStateToString, InMobileTypes.deviceStateToString  depending on your use case instead.");

/**
 * Converts an AuthState to its NSString representation
 * @param state An AuthState value.
 * @return NSString of the requested state value.
 **/
+ (NSString *)authStateToString:(AuthState)state __deprecated_msg("as of v9.0. Use InMobileTypes.authStateToString instead.");

/**
 * Converts an MMELogSet to its NSString representation
 * @param logChoiceSet An MMELogSet value.
 * @return NSString of the requested logset value.
 **/
+ (NSString *)logTypeToString:(MMELogSet)logChoiceSet __deprecated_msg("as of v9.0. Use InMobileTypes.logTypeToString instead.");


@end
NS_ASSUME_NONNULL_END
