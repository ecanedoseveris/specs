cordova.define("everis-nfc-passport-reader.Main", function(require, exports, module) {
var exec = require('cordova/exec');

var EverisNfcPassportReader = function() {};

EverisNfcPassportReader.prototype.read = function(passportNumber, birthDate, expiryDate, jsonGUIMessages, onSuccess, onFailure)
{
    cordova.exec(onSuccess, onFailure, "NfcPassportReader", "read", [passportNumber, birthDate, expiryDate, jsonGUIMessages]);
};

// La callback onSuccess devuelve un objeto Json con el campo string NfcAvailable que puede tener los valores KO o OK. 
EverisNfcPassportReader.prototype.isNfcAvailable = function(onSuccess, onFailure)
{
    cordova.exec(onSuccess, onFailure, "NfcPassportReader", "isNfcAvailable", []);
};

EverisNfcPassportReader.prototype.isNfcEnabled = function(onSuccess, onFailure)
{
    onSuccess({ "NfcEnabled": "OK" });
};

module.exports = new EverisNfcPassportReader();


});
