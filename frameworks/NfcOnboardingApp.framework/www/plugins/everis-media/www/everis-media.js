cordova.define("everis-media.Main", function(require, exports, module) {
var exec = require('cordova/exec');

var EverisMicrophone = function() {};
var EverisCamera = function() {};

//   Inicia la captura de muestras de audio y retorna inmediatamente.
// Cuando la sesión de captura se haya iniciado, se recibirá una
// llamada en el manejador "win(result)", donde "result.sampleRate" es el número
// de muestras por segundo (e.g. 44.100).
//   Las muestras capturadas se proporcionarán mediante llamadas asíncronas
// a "onSamplesAvailable(result)", donde "result.b64Samples" es la codificación
// en base 64 de un array de bytes donde cada muestra son 2 bytes (16 bits)
// ordenados según "little-endian". Este manejador es opcional (puede pasarse
// "null"). En ese caso no se recibirán las muestras en tiempo real y
// "startMicrophone()" es simplemente un requisito previo para poder iniciar
// una grabación mediante "startRecordong()".
//   El manejador "win()" siempre se invocará antes que "onSamplesAvailable()".
//   Si se produce algún error al iniciar el micrófono, se llamará a "fail(error)"
// con la descripción del error.
EverisMicrophone.prototype.startMicrophone = function(win, fail, onSamplesAvailable)
{
    var callback = function(result) {
        if(result && result.reason == "samples") {
            onSamplesAvailable(result);
        }
        else {
            win(result);
        }
    };

    var fprovideRealTimeSamples = (typeof onSamplesAvailable === "function");

    cordova.exec(callback, fail, "Microphone", "startMicrophone",
        [fprovideRealTimeSamples]
    );
};

EverisMicrophone.prototype.stopMicrophone = function(win, fail)
{
    cordova.exec(win, fail, "Microphone", "stopMicrophone", []);
};

//   Inicia la grabación de muestras. El archivo de muestras grabado se
// proporcionará al detener la grabación mediante "stopRecording()".
//   Para poder llamar a "startRecording()" es necesario haber iniciado
// previamente la sesión de captura de muestras mediante "startMicrophone()".
EverisMicrophone.prototype.startRecording = function(win, fail)
{
    cordova.exec(win, fail, "Microphone", "startRecording", []);
};

//   Cuando la grabación se haya detenido completamente, se recibirá una
// llamada en "win(result)", donde "result.b64RecordedData" es un archivo
// WAV codificado en base 64.
EverisMicrophone.prototype.stopRecording = function(win, fail)
{
    cordova.exec(win, fail, "Microphone", "stopRecording", []);
};

EverisCamera.prototype.getCameraList = function(win, fail)
{
    cordova.exec(win, fail, "Camera", "getCameraList", []);
};

EverisCamera.prototype.startCamera = function(data, win, fail)
{
    cordova.exec(win, fail, "Camera", "startCamera", [
        data.cameraId,
        data.x, data.y, data.width, data.height,
        data.behindWebView,
        data.videoOrientation
    ]);
};

EverisCamera.prototype.setPositionAndSize = function(data, win)
{
    cordova.exec(win, function() {}, "Camera", "setPositionAndSize", [
        data.x, data.y, data.width, data.height
    ]);
};

EverisCamera.prototype.stopCamera = function(win, fail)
{
    cordova.exec(win, fail, "Camera", "stopCamera", [ ]);
};

//   El manejador "win(result)" proporcionará los siguientes datos:
// - result.b64JpegData
// - result.b64FullJpeg
EverisCamera.prototype.takePhoto = function(data, win, fail)
{
    cordova.exec(win, fail, "Camera", "takePhoto", [
        data.jpegQuality, data.x, data.y, data.width, data.height, data.desiredWidth
    ]);
};

EverisCamera.prototype.startRecording = function(win, fail)
{
    cordova.exec(win, fail, "Camera", "startRecording", []);
};

EverisCamera.prototype.stopRecording = function(win, fail)
{
    cordova.exec(win, fail, "Camera", "stopRecording", []);
};

module.exports = {
    camera: new EverisCamera(),
    microphone: new EverisMicrophone()
};

});
