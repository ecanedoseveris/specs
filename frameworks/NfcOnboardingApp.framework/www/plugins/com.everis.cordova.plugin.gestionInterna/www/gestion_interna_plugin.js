cordova.define("com.everis.cordova.plugin.gestionInterna.gestion_interna_plugin", function(require, exports, module) {
var exec = require('cordova/exec');

// La callback "win(result)" proporcionará los siguientes datos provenientes de la aplicación contenedora:
// - result.userId (String)
// - result.password (String)
// - result.language (String)
// - result.inputData (String-JSON)
// - result.extraData (String-JSON)
exports.getCredentials = function (win, fail, data) {
    exec(win, fail, 'gestion_interna_plugin', 'getCredentials', data);
};

exports.setWebViewErrorHandler = function (win, fail, data) {
    exec(win, fail, 'gestion_interna_plugin', 'setWebViewErrorHandler', data);
};

//   "data" es objeto que debe contener al menos las dos siguientes propiedades:
// - resultCode (String): número entero en forma de cadena de texto:
//   "-1" -> OK
//    "0" -> Cancelado por el usuario
//    "1" -> ERROR
// - resultMessage (String): descripción del posible error
exports.closeActivity = function (win, fail, data) {

    if(cordova.platformId == "android") {
        exec(win, fail, 'gestion_interna_plugin', 'closeActivity', [ data ]);
    }
    else {
        let jsonOutputData = JSON.stringify(data);
        consoleLog("closeActivity(): " + jsonOutputData);
        exec(win, fail, 'gestion_interna_plugin', 'closeActivity', [ data.resultCode, jsonOutputData ]);
    }
};

// Deletes all cached files and cookies.
exports.clearCache = function(win, fail) {
    exec(win, fail, 'gestion_interna_plugin', 'clearCache', null);
}


});
