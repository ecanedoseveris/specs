cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "es6-promise-plugin.Promise",
      "file": "plugins/es6-promise-plugin/www/promise.js",
      "pluginId": "es6-promise-plugin",
      "runs": true
    },
    {
      "id": "cordova-plugin-android-permissions.Permissions",
      "file": "plugins/cordova-plugin-android-permissions/www/permissions-dummy.js",
      "pluginId": "cordova-plugin-android-permissions",
      "clobbers": [
        "cordova.plugins.permissions"
      ]
    },
    {
      "id": "cordova-plugin-certificates.Certificates",
      "file": "plugins/cordova-plugin-certificates/www/certificate.js",
      "pluginId": "cordova-plugin-certificates",
      "clobbers": [
        "cordova.plugins.certificates"
      ]
    },
    {
      "id": "cordova-plugin-screen-orientation.screenorientation",
      "file": "plugins/cordova-plugin-screen-orientation/www/screenorientation.js",
      "pluginId": "cordova-plugin-screen-orientation",
      "clobbers": [
        "cordova.plugins.screenorientation"
      ]
    },
    {
      "id": "cordova-plugin-sslcertificatechecker.SSLCertificateChecker",
      "file": "plugins/cordova-plugin-sslcertificatechecker/www/SSLCertificateChecker.js",
      "pluginId": "cordova-plugin-sslcertificatechecker",
      "clobbers": [
        "window.plugins.sslCertificateChecker"
      ]
    },
    {
      "id": "cordova-plugin-iroot.IRoot",
      "file": "plugins/cordova-plugin-iroot/www/iroot.js",
      "pluginId": "cordova-plugin-iroot",
      "clobbers": [
        "IRoot"
      ]
    },
    {
      "id": "cordova-plugin-wkwebview-engine.ios-wkwebview-exec",
      "file": "plugins/cordova-plugin-wkwebview-engine/src/www/ios/ios-wkwebview-exec.js",
      "pluginId": "cordova-plugin-wkwebview-engine",
      "clobbers": [
        "cordova.exec"
      ]
    },
    {
      "id": "cordova-plugin-wkwebview-engine.ios-wkwebview",
      "file": "plugins/cordova-plugin-wkwebview-engine/src/www/ios/ios-wkwebview.js",
      "pluginId": "cordova-plugin-wkwebview-engine",
      "clobbers": [
        "window.WkWebView"
      ]
    },
    {
      "id": "cordova-plugin-statusbar.statusbar",
      "file": "plugins/cordova-plugin-statusbar/www/statusbar.js",
      "pluginId": "cordova-plugin-statusbar",
      "clobbers": [
        "window.StatusBar"
      ]
    },
    {
      "id": "everis-nfc-passport-reader.Main",
      "file": "plugins/everis-nfc-passport-reader/www/everis-nfc-passport-reader.js",
      "pluginId": "everis-nfc-passport-reader",
      "clobbers": [
        "passportReader"
      ]
    },
    {
      "id": "com.everis.cordova.plugin.gestionInterna.gestion_interna_plugin",
      "file": "plugins/com.everis.cordova.plugin.gestionInterna/www/gestion_interna_plugin.js",
      "pluginId": "com.everis.cordova.plugin.gestionInterna",
      "clobbers": [
        "gestion_interna_plugin"
      ]
    },
    {
      "id": "everis-media.Main",
      "file": "plugins/everis-media/www/everis-media.js",
      "pluginId": "everis-media",
      "clobbers": [
        "everisMedia"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "es6-promise-plugin": "4.2.2",
    "cordova-plugin-android-permissions": "1.0.0",
    "cordova-plugin-certificates": "0.6.4",
    "cordova-plugin-screen-orientation": "3.0.1",
    "cordova-plugin-sslcertificatechecker": "6.0.0",
    "cordova-plugin-iroot": "0.8.1",
    "cordova-plugin-splashscreen": "6.0.0",
    "cordova-plugin-add-swift-support": "2.0.2",
    "cordova-plugin-wkwebview-engine": "1.2.2-dev",
    "cordova-plugin-statusbar": "2.4.3",
    "everis-nfc-passport-reader": "0.1.0",
    "com.everis.cordova.plugin.gestionInterna": "0.0.1",
    "everis-media": "0.1.0"
  };
});