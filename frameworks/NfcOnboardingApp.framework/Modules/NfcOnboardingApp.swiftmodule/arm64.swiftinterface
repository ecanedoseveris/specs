// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.3.1 (swiftlang-1200.0.41 clang-1200.0.32.8)
// swift-module-flags: -target arm64-apple-ios13.2 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name NfcOnboardingApp
import AVFoundation
import CommonCrypto
import Cordova
import CoreNFC
import CryptoKit
import Foundation
@_exported import NfcOnboardingApp
import OpenSSL
import Swift
import UIKit
import WebKit
public enum CertificateType {
  case documentSigningCertificate
  case issuerSigningCertificate
  public static func == (a: NfcOnboardingApp.CertificateType, b: NfcOnboardingApp.CertificateType) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public enum CertificateItem : Swift.String {
  case fingerprint
  case issuerName
  case subjectName
  case serialNumber
  case signatureAlgorithm
  case publicKeyAlgorithm
  case notBefore
  case notAfter
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
public class X509Wrapper {
  final public let cert: Swift.UnsafeMutablePointer<OpenSSL.X509>
  public init?(with cert: Swift.UnsafeMutablePointer<OpenSSL.X509>?)
  public func getItemsAsDict() -> [NfcOnboardingApp.CertificateItem : Swift.String]
  public func certToPEM() -> Swift.String
  public func getFingerprint() -> Swift.String?
  public func getNotBeforeDate() -> Swift.String?
  public func getNotAfterDate() -> Swift.String?
  public func getSerialNumber() -> Swift.String?
  public func getSignatureAlgorithm() -> Swift.String?
  public func getPublicKeyAlgorithm() -> Swift.String?
  public func getIssuerName() -> Swift.String?
  public func getSubjectName() -> Swift.String?
  @objc deinit
}
public enum OpenSSLError : Swift.Error {
  case UnableToGetX509CertificateFromPKCS7(Swift.String)
  case UnableToVerifyX509CertificateForSOD(Swift.String)
  case UnableToGetSignedDataFromPKCS7(Swift.String)
  case UnableToReadECPublicKey(Swift.String)
  case UnableToExtractSignedDataFromPKCS7(Swift.String)
  case UnableToParseASN1(Swift.String)
}
extension OpenSSLError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
@available(iOS 13, *)
public class SecureMessaging {
  public init(ksenc: [Swift.UInt8], ksmac: [Swift.UInt8], ssc: [Swift.UInt8])
  @objc deinit
}
public func binToHexRep(_ val: [Swift.UInt8]) -> Swift.String
public func binToHexRep(_ val: Swift.UInt8) -> Swift.String
public func binToHex(_ val: Swift.UInt8) -> Swift.Int
public func binToHex(_ val: [Swift.UInt8]) -> Swift.UInt64
public func binToHex(_ val: Swift.ArraySlice<Swift.UInt8>) -> Swift.UInt64
public func hexToBin(_ val: Swift.UInt64) -> [Swift.UInt8]
public func binToInt(_ val: Swift.ArraySlice<Swift.UInt8>) -> Swift.Int
public func binToInt(_ val: [Swift.UInt8]) -> Swift.Int
public func intToBin(_ data: Swift.Int, pad: Swift.Int = 2) -> [Swift.UInt8]
public func hexRepToBin(_ val: Swift.String) -> [Swift.UInt8]
public func xor(_ kifd: [Swift.UInt8], _ response_kicc: [Swift.UInt8]) -> [Swift.UInt8]
public func generateRandomUInt8Array(_ size: Swift.Int) -> [Swift.UInt8]
public func pad(_ toPad: [Swift.UInt8]) -> [Swift.UInt8]
public func unpad(_ tounpad: [Swift.UInt8]) -> [Swift.UInt8]
@available(iOS 13, *)
public func mac(key: [Swift.UInt8], msg: [Swift.UInt8]) -> [Swift.UInt8]
@available(iOS 13, *)
public func asn1Length(_ data: Swift.ArraySlice<Swift.UInt8>) throws -> (Swift.Int, Swift.Int)
@available(iOS 13, *)
public func asn1Length(_ data: [Swift.UInt8]) throws -> (Swift.Int, Swift.Int)
@available(iOS 13, *)
public func toAsn1Length(_ data: Swift.Int) throws -> [Swift.UInt8]
public enum PassiveAuthenticationError : Swift.Error {
  case UnableToParseSODHashes(Swift.String)
  case InvalidDataGroupHash(Swift.String)
  case SODMissing(Swift.String)
}
extension PassiveAuthenticationError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
@_inheritsConvenienceInitializers @objc(EverisInternalManagementPlugin) public class EverisInternalManagementPlugin : Cordova.CDVPlugin {
  @objc deinit
  @objc override dynamic public init()
}
@_inheritsConvenienceInitializers @objc(EverisCameraPlugin) public class EverisCameraPlugin : Cordova.CDVPlugin {
  @objc override dynamic public func pluginInitialize()
  @objc deinit
  @objc override dynamic public init()
}
@objc @_hasMissingDesignatedInitializers public class CameraManager : ObjectiveC.NSObject, AVFoundation.AVCaptureVideoDataOutputSampleBufferDelegate, AVFoundation.AVCapturePhotoCaptureDelegate {
  public func updateVideoLayerBounds()
  public static func getCameraById(_ cameraID: Swift.String) -> AVFoundation.AVCaptureDevice?
  public func startVideoRecord(videoBaseUrl: Foundation.URL, chunkMaxDuration: Swift.Float? = nil, size: (width: Swift.Int, height: Swift.Int)? = nil, fileType: AVFoundation.AVFileType = .mp4, chunkCompletionHandler: ((Swift.String, Foundation.URL?, Swift.Error?) -> (Swift.Void))? = nil)
  @objc public func captureOutput(_ captureOutput: AVFoundation.AVCaptureOutput, didOutput sampleBuffer: CoreMedia.CMSampleBuffer, from connection: AVFoundation.AVCaptureConnection)
  @objc public func photoOutput(_ output: AVFoundation.AVCapturePhotoOutput, didFinishCaptureFor resolvedSettings: AVFoundation.AVCaptureResolvedPhotoSettings, error: Swift.Error?)
  @objc public func photoOutput(_ output: AVFoundation.AVCapturePhotoOutput, willCapturePhotoFor resolvedSettings: AVFoundation.AVCaptureResolvedPhotoSettings)
  @objc public func photoOutput(_ output: AVFoundation.AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVFoundation.AVCapturePhoto, error: Swift.Error?)
  public func processPhotogram(photo: AVFoundation.AVCapturePhoto, error: Swift.Error?, notify: Swift.Bool)
  @objc deinit
  @objc override dynamic public init()
}
@objc @_hasMissingDesignatedInitializers public class EverisMicrophoneManager : ObjectiveC.NSObject, AVFoundation.AVCaptureAudioDataOutputSampleBufferDelegate {
  @objc public func captureOutput(_ captureOutput: AVFoundation.AVCaptureOutput, didOutput sampleBuffer: CoreMedia.CMSampleBuffer, from connection: AVFoundation.AVCaptureConnection)
  @objc deinit
  @objc override dynamic public init()
}
@available(iOS 13, *)
public class BACHandler {
  public var ksenc: [Swift.UInt8]
  public var ksmac: [Swift.UInt8]
  public var kifd: [Swift.UInt8]
  public init()
  public init(tagReader: NfcOnboardingApp.TagReader)
  public func performBACAndGetSessionKeys(mrzKey: Swift.String, completed: @escaping (NfcOnboardingApp.TagError?) -> ())
  public func sessionKeys(data: [Swift.UInt8]) -> ([Swift.UInt8], [Swift.UInt8], [Swift.UInt8])
  @objc deinit
}
@available(iOS 13, *)
public struct DataGroupHash {
  public var id: Swift.String
  public var sodHash: Swift.String
  public var computedHash: Swift.String
  public var match: Swift.Bool
}
@available(iOS 13, *)
public class NFCPassportModel {
  public var documentType: Swift.String {
    get
    set
  }
  public var documentSubType: Swift.String {
    get
    set
  }
  public var personalNumber: Swift.String {
    get
    set
  }
  public var documentNumber: Swift.String {
    get
    set
  }
  public var issuingAuthority: Swift.String {
    get
    set
  }
  public var documentExpiryDate: Swift.String {
    get
    set
  }
  public var dateOfBirth: Swift.String {
    get
    set
  }
  public var gender: Swift.String {
    get
    set
  }
  public var nationality: Swift.String {
    get
    set
  }
  public var lastName: Swift.String {
    get
    set
  }
  public var firstName: Swift.String {
    get
    set
  }
  public var passportMRZ: Swift.String {
    get
    set
  }
  public var documentSigningCertificate: NfcOnboardingApp.X509Wrapper? {
    get
    set
  }
  public var countrySigningCertificate: NfcOnboardingApp.X509Wrapper? {
    get
    set
  }
  public var LDSVersion: Swift.String {
    get
    set
  }
  public var dataGroupsPresent: [Swift.String] {
    get
    set
  }
  public var readDataGroups: [Swift.String]
  public var dataGroupHashes: [NfcOnboardingApp.DataGroupId : NfcOnboardingApp.DataGroupHash]
  public var passportCorrectlySigned: Swift.Bool
  public var documentSigningCertificateVerified: Swift.Bool
  public var passportDataNotTampered: Swift.Bool
  public var activeAuthenticationPassed: Swift.Bool
  public var verificationErrors: [Swift.Error]
  public var passportImage: UIKit.UIImage? {
    get
  }
  public var signatureImage: UIKit.UIImage? {
    get
  }
  public var activeAuthenticationSupported: Swift.Bool {
    get
  }
  public init()
  public func addDataGroup(_ id: NfcOnboardingApp.DataGroupId, dataGroup: NfcOnboardingApp.DataGroup)
  public func getDataGroup(_ id: NfcOnboardingApp.DataGroupId) -> NfcOnboardingApp.DataGroup?
  public func getHashesForDatagroups(hashAlgorythm: Swift.String) -> [NfcOnboardingApp.DataGroupId : [Swift.UInt8]]
  public func verifyPassport(masterListURL: Foundation.URL)
  public func verifyActiveAuthentication(challenge: [Swift.UInt8], signature: [Swift.UInt8])
  @objc deinit
}
@available(iOS 13, *)
public func tripleDESEncrypt(key: [Swift.UInt8], message: [Swift.UInt8], iv: [Swift.UInt8]) -> [Swift.UInt8]
@available(iOS 13, *)
public func tripleDESDecrypt(key: [Swift.UInt8], message: [Swift.UInt8], iv: [Swift.UInt8]) -> [Swift.UInt8]
@available(iOS 13, *)
public func DESEncrypt(key: [Swift.UInt8], message: [Swift.UInt8], iv: [Swift.UInt8], options: Swift.UInt32 = 0) -> [Swift.UInt8]
@available(iOS 13, *)
public func DESDecrypt(key: [Swift.UInt8], message: [Swift.UInt8], iv: [Swift.UInt8], options: Swift.UInt32 = 0) -> [Swift.UInt8]
@_inheritsConvenienceInitializers @objc(EverisMicrophonePlugin) public class EverisMicrophonePlugin : Cordova.CDVPlugin, AVFoundation.AVCaptureAudioDataOutputSampleBufferDelegate {
  @objc override dynamic public func pluginInitialize()
  @objc deinit
  @objc override dynamic public init()
}
@objc @available(iOS 13, *)
public class PassportReader : ObjectiveC.NSObject {
  public init(masterListURL: Foundation.URL? = nil)
  public func setMasterListURL(_ masterListURL: Foundation.URL)
  public func readPassport(mrzKey: Swift.String, tags: [NfcOnboardingApp.DataGroupId] = [], skipSecureElements: Swift.Bool = true, completed: @escaping (NfcOnboardingApp.NFCPassportModel?, NfcOnboardingApp.TagError?) -> (), guiMessages: [Swift.String : Swift.String])
  @objc deinit
  @objc override dynamic public init()
}
@available(iOS 13, *)
extension PassportReader : CoreNFC.NFCTagReaderSessionDelegate {
  public func tagReaderSessionDidBecomeActive(_ session: CoreNFC.NFCTagReaderSession)
  public func tagReaderSession(_ session: CoreNFC.NFCTagReaderSession, didInvalidateWithError error: Swift.Error)
  public func tagReaderSession(_ session: CoreNFC.NFCTagReaderSession, didDetect tags: [CoreNFC.NFCTag])
}
public enum LogLevel : Swift.Int {
  case verbose
  case debug
  case info
  case warning
  case error
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
@_hasMissingDesignatedInitializers public class Log {
  public static var logLevel: NfcOnboardingApp.LogLevel
  public class func verbose(_ msg: Swift.String)
  public class func debug(_ msg: Swift.String)
  public class func info(_ msg: Swift.String)
  public class func warning(_ msg: Swift.String)
  public class func error(_ msg: Swift.String)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class NfcOnboardingApp {
  public static func getViewController(userId: Swift.String, password: Swift.String, language: Swift.String, inputData: Swift.String, extraData: Swift.String, completionHandler: ((Swift.Int, Swift.String) -> Swift.Void)?) -> UIKit.UIViewController
  @objc deinit
}
@_inheritsConvenienceInitializers @available(iOS 13, *)
@objc(EverisNfcPassportReaderPlugin) public class EverisNfcPassportReaderPlugin : Cordova.CDVPlugin {
  @objc deinit
  @objc override dynamic public init()
}
@objc @_hasMissingDesignatedInitializers public class EverisCameraUIViewController : UIKit.UIViewController {
  public init(_ rectPreview: CoreGraphics.CGRect, _ strCameraId: Swift.String, videoOrientation: Swift.String, completionHandler: ((Swift.String, Swift.Error?) -> (Swift.Void))? = nil)
  public func getCameraManager() -> NfcOnboardingApp.CameraManager?
  @objc override dynamic public func loadView()
  @objc override dynamic public func viewDidLoad()
  @objc override dynamic public func viewDidAppear(_ animated: Swift.Bool)
  @objc override dynamic public func viewWillDisappear(_ animated: Swift.Bool)
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc deinit
}
@available(iOS 13, *)
public enum PassportTagError : Swift.Error {
  case responseError(Swift.UInt8, Swift.UInt8)
}
@available(iOS 13, *)
extension PassportTagError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
@available(iOS 13, *)
public enum TagError : Swift.Error {
  case ResponseError(Swift.String)
  case InvalidResponse
  case UnexpectedError
  case NFCNotSupported
  case NoConnectedTag
  case D087Malformed
  case InvalidResponseChecksum
  case MissingMandatoryFields
  case CannotDecodeASN1Length
  case InvalidASN1Value
  case UnableToProtectAPDU
  case UnableToUnprotectAPDU
  case UnsupportedDataGroup
  case DataGroupNotRead
  case UnknownTag
  case UnknownImageFormat
  case NotImplemented
  case Cancelled
  case ErrorAuthentication
}
@available(iOS 13, *)
public enum DataGroupId : Swift.Int, Swift.CaseIterable {
  case COM
  case DG1
  case DG2
  case DG3
  case DG4
  case DG5
  case DG6
  case DG7
  case DG8
  case DG9
  case DG10
  case DG11
  case DG12
  case DG13
  case DG14
  case DG15
  case DG16
  case SOD
  case Unknown
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
  public typealias AllCases = [NfcOnboardingApp.DataGroupId]
  public static var allCases: [NfcOnboardingApp.DataGroupId] {
    get
  }
}
@available(iOS 13, *)
public struct ResponseAPDU {
  public var data: [Swift.UInt8]
  public var sw1: Swift.UInt8
  public var sw2: Swift.UInt8
  public init(data: [Swift.UInt8], sw1: Swift.UInt8, sw2: Swift.UInt8)
}
@_hasMissingDesignatedInitializers @available(iOS 13, *)
public class TagReader {
  @objc deinit
}
@_hasMissingDesignatedInitializers @available(iOS 13, *)
public class DataGroup {
  public var datagroupType: NfcOnboardingApp.DataGroupId
  public var body: [Swift.UInt8]
  public var data: [Swift.UInt8]
  public func hash(_ hashAlgorythm: Swift.String) -> [Swift.UInt8]
  @objc deinit
}
@available(iOS 13, *)
public enum DocTypeEnum : Swift.String {
  case TD1
  case TD2
  case OTHER
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
extension NfcOnboardingApp.CertificateType : Swift.Equatable {}
extension NfcOnboardingApp.CertificateType : Swift.Hashable {}
extension NfcOnboardingApp.CertificateItem : Swift.Equatable {}
extension NfcOnboardingApp.CertificateItem : Swift.Hashable {}
extension NfcOnboardingApp.CertificateItem : Swift.RawRepresentable {}
extension NfcOnboardingApp.LogLevel : Swift.Equatable {}
extension NfcOnboardingApp.LogLevel : Swift.Hashable {}
extension NfcOnboardingApp.LogLevel : Swift.RawRepresentable {}
@available(iOS 13, *)
extension NfcOnboardingApp.DataGroupId : Swift.Equatable {}
@available(iOS 13, *)
extension NfcOnboardingApp.DataGroupId : Swift.Hashable {}
@available(iOS 13, *)
extension NfcOnboardingApp.DataGroupId : Swift.RawRepresentable {}
@available(iOS 13, *)
extension NfcOnboardingApp.DocTypeEnum : Swift.Equatable {}
@available(iOS 13, *)
extension NfcOnboardingApp.DocTypeEnum : Swift.Hashable {}
@available(iOS 13, *)
extension NfcOnboardingApp.DocTypeEnum : Swift.RawRepresentable {}
