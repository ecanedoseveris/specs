//
//  NfcOnboardingApp.h
//  NfcOnboardingApp
//
//  Created by Identity Adssegid on 02/03/2020.
//  Copyright © 2020 Everis Aeroespacial, Defensa y Seguridad. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for NfcOnboardingApp.
FOUNDATION_EXPORT double NfcOnboardingAppVersionNumber;

//! Project version string for NfcOnboardingApp.
FOUNDATION_EXPORT const unsigned char NfcOnboardingAppVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NfcOnboardingApp/PublicHeader.h>

#import <Cordova/Cordova.h>
